<?php

namespace IiMedias\AddressBundle\Model;

use IiMedias\AddressBundle\Model\Base\BanoContent as BaseBanoContent;

/**
 * Skeleton subclass for representing a row from the 'address_bano_content_adbctn' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BanoContent extends BaseBanoContent
{

}
