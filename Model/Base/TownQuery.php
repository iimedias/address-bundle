<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\Town as ChildTown;
use IiMedias\AddressBundle\Model\TownQuery as ChildTownQuery;
use IiMedias\AddressBundle\Model\Map\TownTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'address_town_adtown' table.
 *
 *
 *
 * @method     ChildTownQuery orderById($order = Criteria::ASC) Order by the adtown_id column
 * @method     ChildTownQuery orderByType($order = Criteria::ASC) Order by the adtown_type column
 * @method     ChildTownQuery orderByName($order = Criteria::ASC) Order by the adtown_name column
 * @method     ChildTownQuery orderByPostCode($order = Criteria::ASC) Order by the adtown_postcode column
 * @method     ChildTownQuery orderByLat($order = Criteria::ASC) Order by the adtown_lat column
 * @method     ChildTownQuery orderByLon($order = Criteria::ASC) Order by the adtown_lon column
 * @method     ChildTownQuery orderByCity($order = Criteria::ASC) Order by the adtown_city column
 * @method     ChildTownQuery orderByDepartement($order = Criteria::ASC) Order by the adtown_departement column
 * @method     ChildTownQuery orderByRegion($order = Criteria::ASC) Order by the adtown_region column
 * @method     ChildTownQuery orderByPop($order = Criteria::ASC) Order by the adtown_pop column
 * @method     ChildTownQuery orderByAdmWeight($order = Criteria::ASC) Order by the adtown_adm_weight column
 * @method     ChildTownQuery orderByImportance($order = Criteria::ASC) Order by the adtown_importance column
 * @method     ChildTownQuery orderByCreatedAt($order = Criteria::ASC) Order by the adtown_created_at column
 * @method     ChildTownQuery orderByUpdatedAt($order = Criteria::ASC) Order by the adtown_updated_at column
 *
 * @method     ChildTownQuery groupById() Group by the adtown_id column
 * @method     ChildTownQuery groupByType() Group by the adtown_type column
 * @method     ChildTownQuery groupByName() Group by the adtown_name column
 * @method     ChildTownQuery groupByPostCode() Group by the adtown_postcode column
 * @method     ChildTownQuery groupByLat() Group by the adtown_lat column
 * @method     ChildTownQuery groupByLon() Group by the adtown_lon column
 * @method     ChildTownQuery groupByCity() Group by the adtown_city column
 * @method     ChildTownQuery groupByDepartement() Group by the adtown_departement column
 * @method     ChildTownQuery groupByRegion() Group by the adtown_region column
 * @method     ChildTownQuery groupByPop() Group by the adtown_pop column
 * @method     ChildTownQuery groupByAdmWeight() Group by the adtown_adm_weight column
 * @method     ChildTownQuery groupByImportance() Group by the adtown_importance column
 * @method     ChildTownQuery groupByCreatedAt() Group by the adtown_created_at column
 * @method     ChildTownQuery groupByUpdatedAt() Group by the adtown_updated_at column
 *
 * @method     ChildTownQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTownQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTownQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTownQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTownQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTownQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTownQuery leftJoinStreet($relationAlias = null) Adds a LEFT JOIN clause to the query using the Street relation
 * @method     ChildTownQuery rightJoinStreet($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Street relation
 * @method     ChildTownQuery innerJoinStreet($relationAlias = null) Adds a INNER JOIN clause to the query using the Street relation
 *
 * @method     ChildTownQuery joinWithStreet($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Street relation
 *
 * @method     ChildTownQuery leftJoinWithStreet() Adds a LEFT JOIN clause and with to the query using the Street relation
 * @method     ChildTownQuery rightJoinWithStreet() Adds a RIGHT JOIN clause and with to the query using the Street relation
 * @method     ChildTownQuery innerJoinWithStreet() Adds a INNER JOIN clause and with to the query using the Street relation
 *
 * @method     \IiMedias\AddressBundle\Model\StreetQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTown findOne(ConnectionInterface $con = null) Return the first ChildTown matching the query
 * @method     ChildTown findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTown matching the query, or a new ChildTown object populated from the query conditions when no match is found
 *
 * @method     ChildTown findOneById(int $adtown_id) Return the first ChildTown filtered by the adtown_id column
 * @method     ChildTown findOneByType(string $adtown_type) Return the first ChildTown filtered by the adtown_type column
 * @method     ChildTown findOneByName(string $adtown_name) Return the first ChildTown filtered by the adtown_name column
 * @method     ChildTown findOneByPostCode(string $adtown_postcode) Return the first ChildTown filtered by the adtown_postcode column
 * @method     ChildTown findOneByLat(double $adtown_lat) Return the first ChildTown filtered by the adtown_lat column
 * @method     ChildTown findOneByLon(double $adtown_lon) Return the first ChildTown filtered by the adtown_lon column
 * @method     ChildTown findOneByCity(string $adtown_city) Return the first ChildTown filtered by the adtown_city column
 * @method     ChildTown findOneByDepartement(string $adtown_departement) Return the first ChildTown filtered by the adtown_departement column
 * @method     ChildTown findOneByRegion(string $adtown_region) Return the first ChildTown filtered by the adtown_region column
 * @method     ChildTown findOneByPop(double $adtown_pop) Return the first ChildTown filtered by the adtown_pop column
 * @method     ChildTown findOneByAdmWeight(double $adtown_adm_weight) Return the first ChildTown filtered by the adtown_adm_weight column
 * @method     ChildTown findOneByImportance(double $adtown_importance) Return the first ChildTown filtered by the adtown_importance column
 * @method     ChildTown findOneByCreatedAt(string $adtown_created_at) Return the first ChildTown filtered by the adtown_created_at column
 * @method     ChildTown findOneByUpdatedAt(string $adtown_updated_at) Return the first ChildTown filtered by the adtown_updated_at column *

 * @method     ChildTown requirePk($key, ConnectionInterface $con = null) Return the ChildTown by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOne(ConnectionInterface $con = null) Return the first ChildTown matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTown requireOneById(int $adtown_id) Return the first ChildTown filtered by the adtown_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByType(string $adtown_type) Return the first ChildTown filtered by the adtown_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByName(string $adtown_name) Return the first ChildTown filtered by the adtown_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByPostCode(string $adtown_postcode) Return the first ChildTown filtered by the adtown_postcode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByLat(double $adtown_lat) Return the first ChildTown filtered by the adtown_lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByLon(double $adtown_lon) Return the first ChildTown filtered by the adtown_lon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByCity(string $adtown_city) Return the first ChildTown filtered by the adtown_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByDepartement(string $adtown_departement) Return the first ChildTown filtered by the adtown_departement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByRegion(string $adtown_region) Return the first ChildTown filtered by the adtown_region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByPop(double $adtown_pop) Return the first ChildTown filtered by the adtown_pop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByAdmWeight(double $adtown_adm_weight) Return the first ChildTown filtered by the adtown_adm_weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByImportance(double $adtown_importance) Return the first ChildTown filtered by the adtown_importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByCreatedAt(string $adtown_created_at) Return the first ChildTown filtered by the adtown_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTown requireOneByUpdatedAt(string $adtown_updated_at) Return the first ChildTown filtered by the adtown_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTown[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTown objects based on current ModelCriteria
 * @method     ChildTown[]|ObjectCollection findById(int $adtown_id) Return ChildTown objects filtered by the adtown_id column
 * @method     ChildTown[]|ObjectCollection findByType(string $adtown_type) Return ChildTown objects filtered by the adtown_type column
 * @method     ChildTown[]|ObjectCollection findByName(string $adtown_name) Return ChildTown objects filtered by the adtown_name column
 * @method     ChildTown[]|ObjectCollection findByPostCode(string $adtown_postcode) Return ChildTown objects filtered by the adtown_postcode column
 * @method     ChildTown[]|ObjectCollection findByLat(double $adtown_lat) Return ChildTown objects filtered by the adtown_lat column
 * @method     ChildTown[]|ObjectCollection findByLon(double $adtown_lon) Return ChildTown objects filtered by the adtown_lon column
 * @method     ChildTown[]|ObjectCollection findByCity(string $adtown_city) Return ChildTown objects filtered by the adtown_city column
 * @method     ChildTown[]|ObjectCollection findByDepartement(string $adtown_departement) Return ChildTown objects filtered by the adtown_departement column
 * @method     ChildTown[]|ObjectCollection findByRegion(string $adtown_region) Return ChildTown objects filtered by the adtown_region column
 * @method     ChildTown[]|ObjectCollection findByPop(double $adtown_pop) Return ChildTown objects filtered by the adtown_pop column
 * @method     ChildTown[]|ObjectCollection findByAdmWeight(double $adtown_adm_weight) Return ChildTown objects filtered by the adtown_adm_weight column
 * @method     ChildTown[]|ObjectCollection findByImportance(double $adtown_importance) Return ChildTown objects filtered by the adtown_importance column
 * @method     ChildTown[]|ObjectCollection findByCreatedAt(string $adtown_created_at) Return ChildTown objects filtered by the adtown_created_at column
 * @method     ChildTown[]|ObjectCollection findByUpdatedAt(string $adtown_updated_at) Return ChildTown objects filtered by the adtown_updated_at column
 * @method     ChildTown[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TownQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AddressBundle\Model\Base\TownQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AddressBundle\\Model\\Town', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTownQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTownQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTownQuery) {
            return $criteria;
        }
        $query = new ChildTownQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTown|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TownTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TownTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTown A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT adtown_id, adtown_type, adtown_name, adtown_postcode, adtown_lat, adtown_lon, adtown_city, adtown_departement, adtown_region, adtown_pop, adtown_adm_weight, adtown_importance, adtown_created_at, adtown_updated_at FROM address_town_adtown WHERE adtown_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTown $obj */
            $obj = new ChildTown();
            $obj->hydrate($row);
            TownTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTown|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the adtown_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE adtown_id = 1234
     * $query->filterById(array(12, 34)); // WHERE adtown_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE adtown_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $id, $comparison);
    }

    /**
     * Filter the query on the adtown_type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE adtown_type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE adtown_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the adtown_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE adtown_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE adtown_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the adtown_postcode column
     *
     * Example usage:
     * <code>
     * $query->filterByPostCode('fooValue');   // WHERE adtown_postcode = 'fooValue'
     * $query->filterByPostCode('%fooValue%'); // WHERE adtown_postcode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByPostCode($postCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_POSTCODE, $postCode, $comparison);
    }

    /**
     * Filter the query on the adtown_lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE adtown_lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE adtown_lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE adtown_lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the adtown_lon column
     *
     * Example usage:
     * <code>
     * $query->filterByLon(1234); // WHERE adtown_lon = 1234
     * $query->filterByLon(array(12, 34)); // WHERE adtown_lon IN (12, 34)
     * $query->filterByLon(array('min' => 12)); // WHERE adtown_lon > 12
     * </code>
     *
     * @param     mixed $lon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByLon($lon = null, $comparison = null)
    {
        if (is_array($lon)) {
            $useMinMax = false;
            if (isset($lon['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_LON, $lon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lon['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_LON, $lon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_LON, $lon, $comparison);
    }

    /**
     * Filter the query on the adtown_city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE adtown_city = 'fooValue'
     * $query->filterByCity('%fooValue%'); // WHERE adtown_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the adtown_departement column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartement('fooValue');   // WHERE adtown_departement = 'fooValue'
     * $query->filterByDepartement('%fooValue%'); // WHERE adtown_departement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByDepartement($departement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_DEPARTEMENT, $departement, $comparison);
    }

    /**
     * Filter the query on the adtown_region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE adtown_region = 'fooValue'
     * $query->filterByRegion('%fooValue%'); // WHERE adtown_region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the adtown_pop column
     *
     * Example usage:
     * <code>
     * $query->filterByPop(1234); // WHERE adtown_pop = 1234
     * $query->filterByPop(array(12, 34)); // WHERE adtown_pop IN (12, 34)
     * $query->filterByPop(array('min' => 12)); // WHERE adtown_pop > 12
     * </code>
     *
     * @param     mixed $pop The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByPop($pop = null, $comparison = null)
    {
        if (is_array($pop)) {
            $useMinMax = false;
            if (isset($pop['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_POP, $pop['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pop['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_POP, $pop['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_POP, $pop, $comparison);
    }

    /**
     * Filter the query on the adtown_adm_weight column
     *
     * Example usage:
     * <code>
     * $query->filterByAdmWeight(1234); // WHERE adtown_adm_weight = 1234
     * $query->filterByAdmWeight(array(12, 34)); // WHERE adtown_adm_weight IN (12, 34)
     * $query->filterByAdmWeight(array('min' => 12)); // WHERE adtown_adm_weight > 12
     * </code>
     *
     * @param     mixed $admWeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByAdmWeight($admWeight = null, $comparison = null)
    {
        if (is_array($admWeight)) {
            $useMinMax = false;
            if (isset($admWeight['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_ADM_WEIGHT, $admWeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($admWeight['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_ADM_WEIGHT, $admWeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_ADM_WEIGHT, $admWeight, $comparison);
    }

    /**
     * Filter the query on the adtown_importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE adtown_importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE adtown_importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE adtown_importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the adtown_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE adtown_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE adtown_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE adtown_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the adtown_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE adtown_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE adtown_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE adtown_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TownTableMap::COL_ADTOWN_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\Street object
     *
     * @param \IiMedias\AddressBundle\Model\Street|ObjectCollection $street the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTownQuery The current query, for fluid interface
     */
    public function filterByStreet($street, $comparison = null)
    {
        if ($street instanceof \IiMedias\AddressBundle\Model\Street) {
            return $this
                ->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $street->getTownId(), $comparison);
        } elseif ($street instanceof ObjectCollection) {
            return $this
                ->useStreetQuery()
                ->filterByPrimaryKeys($street->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStreet() only accepts arguments of type \IiMedias\AddressBundle\Model\Street or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Street relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function joinStreet($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Street');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Street');
        }

        return $this;
    }

    /**
     * Use the Street relation Street object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\StreetQuery A secondary query class using the current class as primary query
     */
    public function useStreetQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStreet($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Street', '\IiMedias\AddressBundle\Model\StreetQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTown $town Object to remove from the list of results
     *
     * @return $this|ChildTownQuery The current query, for fluid interface
     */
    public function prune($town = null)
    {
        if ($town) {
            $this->addUsingAlias(TownTableMap::COL_ADTOWN_ID, $town->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address_town_adtown table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TownTableMap::clearInstancePool();
            TownTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TownTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TownTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TownTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(TownTableMap::COL_ADTOWN_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(TownTableMap::COL_ADTOWN_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(TownTableMap::COL_ADTOWN_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(TownTableMap::COL_ADTOWN_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildTownQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(TownTableMap::COL_ADTOWN_CREATED_AT);
    }

} // TownQuery
