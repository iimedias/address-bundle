<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\City as ChildCity;
use IiMedias\AddressBundle\Model\CityQuery as ChildCityQuery;
use IiMedias\AddressBundle\Model\County as ChildCounty;
use IiMedias\AddressBundle\Model\CountyQuery as ChildCountyQuery;
use IiMedias\AddressBundle\Model\Street as ChildStreet;
use IiMedias\AddressBundle\Model\StreetQuery as ChildStreetQuery;
use IiMedias\AddressBundle\Model\Type as ChildType;
use IiMedias\AddressBundle\Model\TypeQuery as ChildTypeQuery;
use IiMedias\AddressBundle\Model\Map\CityTableMap;
use IiMedias\AddressBundle\Model\Map\StreetTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'address_city_adcity' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.AddressBundle.Model.Base
 */
abstract class City implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\AddressBundle\\Model\\Map\\CityTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the adcity_id field.
     *
     * @var        string
     */
    protected $adcity_id;

    /**
     * The value for the adcity_adtype_id field.
     *
     * @var        int
     */
    protected $adcity_adtype_id;

    /**
     * The value for the adcity_adcnty_id field.
     *
     * @var        int
     */
    protected $adcity_adcnty_id;

    /**
     * The value for the adcity_name field.
     *
     * @var        string
     */
    protected $adcity_name;

    /**
     * The value for the adcity_postcode field.
     *
     * @var        string
     */
    protected $adcity_postcode;

    /**
     * The value for the adcity_alternative_postcode_1 field.
     *
     * @var        string
     */
    protected $adcity_alternative_postcode_1;

    /**
     * The value for the adcity_alternative_postcode_2 field.
     *
     * @var        string
     */
    protected $adcity_alternative_postcode_2;

    /**
     * The value for the adcity_alternative_postcode_3 field.
     *
     * @var        string
     */
    protected $adcity_alternative_postcode_3;

    /**
     * The value for the adcity_lat field.
     *
     * @var        double
     */
    protected $adcity_lat;

    /**
     * The value for the adcity_lon field.
     *
     * @var        double
     */
    protected $adcity_lon;

    /**
     * The value for the adcity_pop field.
     *
     * @var        double
     */
    protected $adcity_pop;

    /**
     * The value for the adcity_adm_weight field.
     *
     * @var        double
     */
    protected $adcity_adm_weight;

    /**
     * The value for the adcity_importance field.
     *
     * @var        double
     */
    protected $adcity_importance;

    /**
     * The value for the adcity_housenumbers_json field.
     *
     * @var        string
     */
    protected $adcity_housenumbers_json;

    /**
     * @var        ChildType
     */
    protected $aType;

    /**
     * @var        ChildCounty
     */
    protected $aCounty;

    /**
     * @var        ObjectCollection|ChildStreet[] Collection to store aggregation of ChildStreet objects.
     */
    protected $collStreets;
    protected $collStreetsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildStreet[]
     */
    protected $streetsScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\AddressBundle\Model\Base\City object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>City</code> instance.  If
     * <code>obj</code> is an instance of <code>City</code>, delegates to
     * <code>equals(City)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|City The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [adcity_id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->adcity_id;
    }

    /**
     * Get the [adcity_adtype_id] column value.
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->adcity_adtype_id;
    }

    /**
     * Get the [adcity_adcnty_id] column value.
     *
     * @return int
     */
    public function getCountyId()
    {
        return $this->adcity_adcnty_id;
    }

    /**
     * Get the [adcity_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->adcity_name;
    }

    /**
     * Get the [adcity_postcode] column value.
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->adcity_postcode;
    }

    /**
     * Get the [adcity_alternative_postcode_1] column value.
     *
     * @return string
     */
    public function getAlternativePostCode1()
    {
        return $this->adcity_alternative_postcode_1;
    }

    /**
     * Get the [adcity_alternative_postcode_2] column value.
     *
     * @return string
     */
    public function getAlternativePostCode2()
    {
        return $this->adcity_alternative_postcode_2;
    }

    /**
     * Get the [adcity_alternative_postcode_3] column value.
     *
     * @return string
     */
    public function getAlternativePostCode3()
    {
        return $this->adcity_alternative_postcode_3;
    }

    /**
     * Get the [adcity_lat] column value.
     *
     * @return double
     */
    public function getLat()
    {
        return $this->adcity_lat;
    }

    /**
     * Get the [adcity_lon] column value.
     *
     * @return double
     */
    public function getLon()
    {
        return $this->adcity_lon;
    }

    /**
     * Get the [adcity_pop] column value.
     *
     * @return double
     */
    public function getPop()
    {
        return $this->adcity_pop;
    }

    /**
     * Get the [adcity_adm_weight] column value.
     *
     * @return double
     */
    public function getAdmWeight()
    {
        return $this->adcity_adm_weight;
    }

    /**
     * Get the [adcity_importance] column value.
     *
     * @return double
     */
    public function getImportance()
    {
        return $this->adcity_importance;
    }

    /**
     * Get the [adcity_housenumbers_json] column value.
     *
     * @return string
     */
    public function getHouseNumbersJson()
    {
        return $this->adcity_housenumbers_json;
    }

    /**
     * Set the value of [adcity_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_id !== $v) {
            $this->adcity_id = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [adcity_adtype_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->adcity_adtype_id !== $v) {
            $this->adcity_adtype_id = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ADTYPE_ID] = true;
        }

        if ($this->aType !== null && $this->aType->getId() !== $v) {
            $this->aType = null;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [adcity_adcnty_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setCountyId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->adcity_adcnty_id !== $v) {
            $this->adcity_adcnty_id = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ADCNTY_ID] = true;
        }

        if ($this->aCounty !== null && $this->aCounty->getId() !== $v) {
            $this->aCounty = null;
        }

        return $this;
    } // setCountyId()

    /**
     * Set the value of [adcity_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_name !== $v) {
            $this->adcity_name = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [adcity_postcode] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setPostCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_postcode !== $v) {
            $this->adcity_postcode = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_POSTCODE] = true;
        }

        return $this;
    } // setPostCode()

    /**
     * Set the value of [adcity_alternative_postcode_1] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setAlternativePostCode1($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_alternative_postcode_1 !== $v) {
            $this->adcity_alternative_postcode_1 = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1] = true;
        }

        return $this;
    } // setAlternativePostCode1()

    /**
     * Set the value of [adcity_alternative_postcode_2] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setAlternativePostCode2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_alternative_postcode_2 !== $v) {
            $this->adcity_alternative_postcode_2 = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2] = true;
        }

        return $this;
    } // setAlternativePostCode2()

    /**
     * Set the value of [adcity_alternative_postcode_3] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setAlternativePostCode3($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_alternative_postcode_3 !== $v) {
            $this->adcity_alternative_postcode_3 = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3] = true;
        }

        return $this;
    } // setAlternativePostCode3()

    /**
     * Set the value of [adcity_lat] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adcity_lat !== $v) {
            $this->adcity_lat = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_LAT] = true;
        }

        return $this;
    } // setLat()

    /**
     * Set the value of [adcity_lon] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setLon($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adcity_lon !== $v) {
            $this->adcity_lon = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_LON] = true;
        }

        return $this;
    } // setLon()

    /**
     * Set the value of [adcity_pop] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setPop($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adcity_pop !== $v) {
            $this->adcity_pop = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_POP] = true;
        }

        return $this;
    } // setPop()

    /**
     * Set the value of [adcity_adm_weight] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setAdmWeight($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adcity_adm_weight !== $v) {
            $this->adcity_adm_weight = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_ADM_WEIGHT] = true;
        }

        return $this;
    } // setAdmWeight()

    /**
     * Set the value of [adcity_importance] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setImportance($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adcity_importance !== $v) {
            $this->adcity_importance = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_IMPORTANCE] = true;
        }

        return $this;
    } // setImportance()

    /**
     * Set the value of [adcity_housenumbers_json] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function setHouseNumbersJson($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adcity_housenumbers_json !== $v) {
            $this->adcity_housenumbers_json = $v;
            $this->modifiedColumns[CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON] = true;
        }

        return $this;
    } // setHouseNumbersJson()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CityTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CityTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_adtype_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CityTableMap::translateFieldName('CountyId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_adcnty_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CityTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CityTableMap::translateFieldName('PostCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_postcode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CityTableMap::translateFieldName('AlternativePostCode1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_alternative_postcode_1 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CityTableMap::translateFieldName('AlternativePostCode2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_alternative_postcode_2 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CityTableMap::translateFieldName('AlternativePostCode3', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_alternative_postcode_3 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CityTableMap::translateFieldName('Lat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_lat = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CityTableMap::translateFieldName('Lon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_lon = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CityTableMap::translateFieldName('Pop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_pop = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CityTableMap::translateFieldName('AdmWeight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_adm_weight = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CityTableMap::translateFieldName('Importance', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_importance = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CityTableMap::translateFieldName('HouseNumbersJson', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adcity_housenumbers_json = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = CityTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\AddressBundle\\Model\\City'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aType !== null && $this->adcity_adtype_id !== $this->aType->getId()) {
            $this->aType = null;
        }
        if ($this->aCounty !== null && $this->adcity_adcnty_id !== $this->aCounty->getId()) {
            $this->aCounty = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CityTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCityQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aType = null;
            $this->aCounty = null;
            $this->collStreets = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see City::setDeleted()
     * @see City::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCityQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CityTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aType !== null) {
                if ($this->aType->isModified() || $this->aType->isNew()) {
                    $affectedRows += $this->aType->save($con);
                }
                $this->setType($this->aType);
            }

            if ($this->aCounty !== null) {
                if ($this->aCounty->isModified() || $this->aCounty->isNew()) {
                    $affectedRows += $this->aCounty->save($con);
                }
                $this->setCounty($this->aCounty);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->streetsScheduledForDeletion !== null) {
                if (!$this->streetsScheduledForDeletion->isEmpty()) {
                    \IiMedias\AddressBundle\Model\StreetQuery::create()
                        ->filterByPrimaryKeys($this->streetsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->streetsScheduledForDeletion = null;
                }
            }

            if ($this->collStreets !== null) {
                foreach ($this->collStreets as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_id';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADTYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_adtype_id';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADCNTY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_adcnty_id';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_name';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_POSTCODE)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_postcode';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_alternative_postcode_1';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_alternative_postcode_2';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_alternative_postcode_3';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_LAT)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_lat';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_LON)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_lon';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_POP)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_pop';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADM_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_adm_weight';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_IMPORTANCE)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_importance';
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON)) {
            $modifiedColumns[':p' . $index++]  = 'adcity_housenumbers_json';
        }

        $sql = sprintf(
            'INSERT INTO address_city_adcity (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'adcity_id':
                        $stmt->bindValue($identifier, $this->adcity_id, PDO::PARAM_STR);
                        break;
                    case 'adcity_adtype_id':
                        $stmt->bindValue($identifier, $this->adcity_adtype_id, PDO::PARAM_INT);
                        break;
                    case 'adcity_adcnty_id':
                        $stmt->bindValue($identifier, $this->adcity_adcnty_id, PDO::PARAM_INT);
                        break;
                    case 'adcity_name':
                        $stmt->bindValue($identifier, $this->adcity_name, PDO::PARAM_STR);
                        break;
                    case 'adcity_postcode':
                        $stmt->bindValue($identifier, $this->adcity_postcode, PDO::PARAM_STR);
                        break;
                    case 'adcity_alternative_postcode_1':
                        $stmt->bindValue($identifier, $this->adcity_alternative_postcode_1, PDO::PARAM_STR);
                        break;
                    case 'adcity_alternative_postcode_2':
                        $stmt->bindValue($identifier, $this->adcity_alternative_postcode_2, PDO::PARAM_STR);
                        break;
                    case 'adcity_alternative_postcode_3':
                        $stmt->bindValue($identifier, $this->adcity_alternative_postcode_3, PDO::PARAM_STR);
                        break;
                    case 'adcity_lat':
                        $stmt->bindValue($identifier, $this->adcity_lat, PDO::PARAM_STR);
                        break;
                    case 'adcity_lon':
                        $stmt->bindValue($identifier, $this->adcity_lon, PDO::PARAM_STR);
                        break;
                    case 'adcity_pop':
                        $stmt->bindValue($identifier, $this->adcity_pop, PDO::PARAM_STR);
                        break;
                    case 'adcity_adm_weight':
                        $stmt->bindValue($identifier, $this->adcity_adm_weight, PDO::PARAM_STR);
                        break;
                    case 'adcity_importance':
                        $stmt->bindValue($identifier, $this->adcity_importance, PDO::PARAM_STR);
                        break;
                    case 'adcity_housenumbers_json':
                        $stmt->bindValue($identifier, $this->adcity_housenumbers_json, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CityTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTypeId();
                break;
            case 2:
                return $this->getCountyId();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getPostCode();
                break;
            case 5:
                return $this->getAlternativePostCode1();
                break;
            case 6:
                return $this->getAlternativePostCode2();
                break;
            case 7:
                return $this->getAlternativePostCode3();
                break;
            case 8:
                return $this->getLat();
                break;
            case 9:
                return $this->getLon();
                break;
            case 10:
                return $this->getPop();
                break;
            case 11:
                return $this->getAdmWeight();
                break;
            case 12:
                return $this->getImportance();
                break;
            case 13:
                return $this->getHouseNumbersJson();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['City'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['City'][$this->hashCode()] = true;
        $keys = CityTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTypeId(),
            $keys[2] => $this->getCountyId(),
            $keys[3] => $this->getName(),
            $keys[4] => $this->getPostCode(),
            $keys[5] => $this->getAlternativePostCode1(),
            $keys[6] => $this->getAlternativePostCode2(),
            $keys[7] => $this->getAlternativePostCode3(),
            $keys[8] => $this->getLat(),
            $keys[9] => $this->getLon(),
            $keys[10] => $this->getPop(),
            $keys[11] => $this->getAdmWeight(),
            $keys[12] => $this->getImportance(),
            $keys[13] => $this->getHouseNumbersJson(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'type';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_type_adtype';
                        break;
                    default:
                        $key = 'Type';
                }

                $result[$key] = $this->aType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCounty) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'county';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_county_adcnty';
                        break;
                    default:
                        $key = 'County';
                }

                $result[$key] = $this->aCounty->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collStreets) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'streets';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_street_adstrts';
                        break;
                    default:
                        $key = 'Streets';
                }

                $result[$key] = $this->collStreets->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\AddressBundle\Model\City
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CityTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\AddressBundle\Model\City
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTypeId($value);
                break;
            case 2:
                $this->setCountyId($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setPostCode($value);
                break;
            case 5:
                $this->setAlternativePostCode1($value);
                break;
            case 6:
                $this->setAlternativePostCode2($value);
                break;
            case 7:
                $this->setAlternativePostCode3($value);
                break;
            case 8:
                $this->setLat($value);
                break;
            case 9:
                $this->setLon($value);
                break;
            case 10:
                $this->setPop($value);
                break;
            case 11:
                $this->setAdmWeight($value);
                break;
            case 12:
                $this->setImportance($value);
                break;
            case 13:
                $this->setHouseNumbersJson($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CityTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTypeId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCountyId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPostCode($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAlternativePostCode1($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAlternativePostCode2($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setAlternativePostCode3($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLat($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLon($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPop($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setAdmWeight($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setImportance($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setHouseNumbersJson($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\AddressBundle\Model\City The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CityTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ID)) {
            $criteria->add(CityTableMap::COL_ADCITY_ID, $this->adcity_id);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADTYPE_ID)) {
            $criteria->add(CityTableMap::COL_ADCITY_ADTYPE_ID, $this->adcity_adtype_id);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADCNTY_ID)) {
            $criteria->add(CityTableMap::COL_ADCITY_ADCNTY_ID, $this->adcity_adcnty_id);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_NAME)) {
            $criteria->add(CityTableMap::COL_ADCITY_NAME, $this->adcity_name);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_POSTCODE)) {
            $criteria->add(CityTableMap::COL_ADCITY_POSTCODE, $this->adcity_postcode);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1)) {
            $criteria->add(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1, $this->adcity_alternative_postcode_1);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2)) {
            $criteria->add(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2, $this->adcity_alternative_postcode_2);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3)) {
            $criteria->add(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3, $this->adcity_alternative_postcode_3);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_LAT)) {
            $criteria->add(CityTableMap::COL_ADCITY_LAT, $this->adcity_lat);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_LON)) {
            $criteria->add(CityTableMap::COL_ADCITY_LON, $this->adcity_lon);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_POP)) {
            $criteria->add(CityTableMap::COL_ADCITY_POP, $this->adcity_pop);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_ADM_WEIGHT)) {
            $criteria->add(CityTableMap::COL_ADCITY_ADM_WEIGHT, $this->adcity_adm_weight);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_IMPORTANCE)) {
            $criteria->add(CityTableMap::COL_ADCITY_IMPORTANCE, $this->adcity_importance);
        }
        if ($this->isColumnModified(CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON)) {
            $criteria->add(CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON, $this->adcity_housenumbers_json);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCityQuery::create();
        $criteria->add(CityTableMap::COL_ADCITY_ID, $this->adcity_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (adcity_id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\AddressBundle\Model\City (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setCountyId($this->getCountyId());
        $copyObj->setName($this->getName());
        $copyObj->setPostCode($this->getPostCode());
        $copyObj->setAlternativePostCode1($this->getAlternativePostCode1());
        $copyObj->setAlternativePostCode2($this->getAlternativePostCode2());
        $copyObj->setAlternativePostCode3($this->getAlternativePostCode3());
        $copyObj->setLat($this->getLat());
        $copyObj->setLon($this->getLon());
        $copyObj->setPop($this->getPop());
        $copyObj->setAdmWeight($this->getAdmWeight());
        $copyObj->setImportance($this->getImportance());
        $copyObj->setHouseNumbersJson($this->getHouseNumbersJson());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getStreets() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addStreet($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\AddressBundle\Model\City Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildType object.
     *
     * @param  ChildType $v
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     * @throws PropelException
     */
    public function setType(ChildType $v = null)
    {
        if ($v === null) {
            $this->setTypeId(NULL);
        } else {
            $this->setTypeId($v->getId());
        }

        $this->aType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildType object, it will not be re-added.
        if ($v !== null) {
            $v->addCity($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildType The associated ChildType object.
     * @throws PropelException
     */
    public function getType(ConnectionInterface $con = null)
    {
        if ($this->aType === null && ($this->adcity_adtype_id !== null)) {
            $this->aType = ChildTypeQuery::create()->findPk($this->adcity_adtype_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aType->addCities($this);
             */
        }

        return $this->aType;
    }

    /**
     * Declares an association between this object and a ChildCounty object.
     *
     * @param  ChildCounty $v
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCounty(ChildCounty $v = null)
    {
        if ($v === null) {
            $this->setCountyId(NULL);
        } else {
            $this->setCountyId($v->getId());
        }

        $this->aCounty = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCounty object, it will not be re-added.
        if ($v !== null) {
            $v->addCity($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCounty object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCounty The associated ChildCounty object.
     * @throws PropelException
     */
    public function getCounty(ConnectionInterface $con = null)
    {
        if ($this->aCounty === null && ($this->adcity_adcnty_id !== null)) {
            $this->aCounty = ChildCountyQuery::create()->findPk($this->adcity_adcnty_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCounty->addCities($this);
             */
        }

        return $this->aCounty;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Street' == $relationName) {
            return $this->initStreets();
        }
    }

    /**
     * Clears out the collStreets collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addStreets()
     */
    public function clearStreets()
    {
        $this->collStreets = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collStreets collection loaded partially.
     */
    public function resetPartialStreets($v = true)
    {
        $this->collStreetsPartial = $v;
    }

    /**
     * Initializes the collStreets collection.
     *
     * By default this just sets the collStreets collection to an empty array (like clearcollStreets());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStreets($overrideExisting = true)
    {
        if (null !== $this->collStreets && !$overrideExisting) {
            return;
        }

        $collectionClassName = StreetTableMap::getTableMap()->getCollectionClassName();

        $this->collStreets = new $collectionClassName;
        $this->collStreets->setModel('\IiMedias\AddressBundle\Model\Street');
    }

    /**
     * Gets an array of ChildStreet objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCity is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildStreet[] List of ChildStreet objects
     * @throws PropelException
     */
    public function getStreets(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collStreetsPartial && !$this->isNew();
        if (null === $this->collStreets || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStreets) {
                // return empty collection
                $this->initStreets();
            } else {
                $collStreets = ChildStreetQuery::create(null, $criteria)
                    ->filterByCity($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collStreetsPartial && count($collStreets)) {
                        $this->initStreets(false);

                        foreach ($collStreets as $obj) {
                            if (false == $this->collStreets->contains($obj)) {
                                $this->collStreets->append($obj);
                            }
                        }

                        $this->collStreetsPartial = true;
                    }

                    return $collStreets;
                }

                if ($partial && $this->collStreets) {
                    foreach ($this->collStreets as $obj) {
                        if ($obj->isNew()) {
                            $collStreets[] = $obj;
                        }
                    }
                }

                $this->collStreets = $collStreets;
                $this->collStreetsPartial = false;
            }
        }

        return $this->collStreets;
    }

    /**
     * Sets a collection of ChildStreet objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $streets A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCity The current object (for fluent API support)
     */
    public function setStreets(Collection $streets, ConnectionInterface $con = null)
    {
        /** @var ChildStreet[] $streetsToDelete */
        $streetsToDelete = $this->getStreets(new Criteria(), $con)->diff($streets);


        $this->streetsScheduledForDeletion = $streetsToDelete;

        foreach ($streetsToDelete as $streetRemoved) {
            $streetRemoved->setCity(null);
        }

        $this->collStreets = null;
        foreach ($streets as $street) {
            $this->addStreet($street);
        }

        $this->collStreets = $streets;
        $this->collStreetsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Street objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Street objects.
     * @throws PropelException
     */
    public function countStreets(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collStreetsPartial && !$this->isNew();
        if (null === $this->collStreets || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStreets) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStreets());
            }

            $query = ChildStreetQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCity($this)
                ->count($con);
        }

        return count($this->collStreets);
    }

    /**
     * Method called to associate a ChildStreet object to this object
     * through the ChildStreet foreign key attribute.
     *
     * @param  ChildStreet $l ChildStreet
     * @return $this|\IiMedias\AddressBundle\Model\City The current object (for fluent API support)
     */
    public function addStreet(ChildStreet $l)
    {
        if ($this->collStreets === null) {
            $this->initStreets();
            $this->collStreetsPartial = true;
        }

        if (!$this->collStreets->contains($l)) {
            $this->doAddStreet($l);

            if ($this->streetsScheduledForDeletion and $this->streetsScheduledForDeletion->contains($l)) {
                $this->streetsScheduledForDeletion->remove($this->streetsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildStreet $street The ChildStreet object to add.
     */
    protected function doAddStreet(ChildStreet $street)
    {
        $this->collStreets[]= $street;
        $street->setCity($this);
    }

    /**
     * @param  ChildStreet $street The ChildStreet object to remove.
     * @return $this|ChildCity The current object (for fluent API support)
     */
    public function removeStreet(ChildStreet $street)
    {
        if ($this->getStreets()->contains($street)) {
            $pos = $this->collStreets->search($street);
            $this->collStreets->remove($pos);
            if (null === $this->streetsScheduledForDeletion) {
                $this->streetsScheduledForDeletion = clone $this->collStreets;
                $this->streetsScheduledForDeletion->clear();
            }
            $this->streetsScheduledForDeletion[]= clone $street;
            $street->setCity(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this City is new, it will return
     * an empty collection; or if this City has previously
     * been saved, it will retrieve related Streets from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in City.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildStreet[] List of ChildStreet objects
     */
    public function getStreetsJoinType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildStreetQuery::create(null, $criteria);
        $query->joinWith('Type', $joinBehavior);

        return $this->getStreets($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aType) {
            $this->aType->removeCity($this);
        }
        if (null !== $this->aCounty) {
            $this->aCounty->removeCity($this);
        }
        $this->adcity_id = null;
        $this->adcity_adtype_id = null;
        $this->adcity_adcnty_id = null;
        $this->adcity_name = null;
        $this->adcity_postcode = null;
        $this->adcity_alternative_postcode_1 = null;
        $this->adcity_alternative_postcode_2 = null;
        $this->adcity_alternative_postcode_3 = null;
        $this->adcity_lat = null;
        $this->adcity_lon = null;
        $this->adcity_pop = null;
        $this->adcity_adm_weight = null;
        $this->adcity_importance = null;
        $this->adcity_housenumbers_json = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collStreets) {
                foreach ($this->collStreets as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collStreets = null;
        $this->aType = null;
        $this->aCounty = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CityTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
