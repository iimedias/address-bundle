<?php

namespace IiMedias\AddressBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\Street as ChildStreet;
use IiMedias\AddressBundle\Model\StreetQuery as ChildStreetQuery;
use IiMedias\AddressBundle\Model\Town as ChildTown;
use IiMedias\AddressBundle\Model\TownQuery as ChildTownQuery;
use IiMedias\AddressBundle\Model\Map\StreetTableMap;
use IiMedias\AddressBundle\Model\Map\TownTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'address_town_adtown' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.AddressBundle.Model.Base
 */
abstract class Town implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\AddressBundle\\Model\\Map\\TownTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the adtown_id field.
     *
     * @var        int
     */
    protected $adtown_id;

    /**
     * The value for the adtown_type field.
     *
     * @var        string
     */
    protected $adtown_type;

    /**
     * The value for the adtown_name field.
     *
     * @var        string
     */
    protected $adtown_name;

    /**
     * The value for the adtown_postcode field.
     *
     * @var        string
     */
    protected $adtown_postcode;

    /**
     * The value for the adtown_lat field.
     *
     * @var        double
     */
    protected $adtown_lat;

    /**
     * The value for the adtown_lon field.
     *
     * @var        double
     */
    protected $adtown_lon;

    /**
     * The value for the adtown_city field.
     *
     * @var        string
     */
    protected $adtown_city;

    /**
     * The value for the adtown_departement field.
     *
     * @var        string
     */
    protected $adtown_departement;

    /**
     * The value for the adtown_region field.
     *
     * @var        string
     */
    protected $adtown_region;

    /**
     * The value for the adtown_pop field.
     *
     * @var        double
     */
    protected $adtown_pop;

    /**
     * The value for the adtown_adm_weight field.
     *
     * @var        double
     */
    protected $adtown_adm_weight;

    /**
     * The value for the adtown_importance field.
     *
     * @var        double
     */
    protected $adtown_importance;

    /**
     * The value for the adtown_created_at field.
     *
     * @var        DateTime
     */
    protected $adtown_created_at;

    /**
     * The value for the adtown_updated_at field.
     *
     * @var        DateTime
     */
    protected $adtown_updated_at;

    /**
     * @var        ObjectCollection|ChildStreet[] Collection to store aggregation of ChildStreet objects.
     */
    protected $collStreets;
    protected $collStreetsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildStreet[]
     */
    protected $streetsScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\AddressBundle\Model\Base\Town object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Town</code> instance.  If
     * <code>obj</code> is an instance of <code>Town</code>, delegates to
     * <code>equals(Town)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Town The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [adtown_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->adtown_id;
    }

    /**
     * Get the [adtown_type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->adtown_type;
    }

    /**
     * Get the [adtown_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->adtown_name;
    }

    /**
     * Get the [adtown_postcode] column value.
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->adtown_postcode;
    }

    /**
     * Get the [adtown_lat] column value.
     *
     * @return double
     */
    public function getLat()
    {
        return $this->adtown_lat;
    }

    /**
     * Get the [adtown_lon] column value.
     *
     * @return double
     */
    public function getLon()
    {
        return $this->adtown_lon;
    }

    /**
     * Get the [adtown_city] column value.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->adtown_city;
    }

    /**
     * Get the [adtown_departement] column value.
     *
     * @return string
     */
    public function getDepartement()
    {
        return $this->adtown_departement;
    }

    /**
     * Get the [adtown_region] column value.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->adtown_region;
    }

    /**
     * Get the [adtown_pop] column value.
     *
     * @return double
     */
    public function getPop()
    {
        return $this->adtown_pop;
    }

    /**
     * Get the [adtown_adm_weight] column value.
     *
     * @return double
     */
    public function getAdmWeight()
    {
        return $this->adtown_adm_weight;
    }

    /**
     * Get the [adtown_importance] column value.
     *
     * @return double
     */
    public function getImportance()
    {
        return $this->adtown_importance;
    }

    /**
     * Get the [optionally formatted] temporal [adtown_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->adtown_created_at;
        } else {
            return $this->adtown_created_at instanceof \DateTimeInterface ? $this->adtown_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [adtown_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->adtown_updated_at;
        } else {
            return $this->adtown_updated_at instanceof \DateTimeInterface ? $this->adtown_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [adtown_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->adtown_id !== $v) {
            $this->adtown_id = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [adtown_type] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_type !== $v) {
            $this->adtown_type = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [adtown_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_name !== $v) {
            $this->adtown_name = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [adtown_postcode] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setPostCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_postcode !== $v) {
            $this->adtown_postcode = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_POSTCODE] = true;
        }

        return $this;
    } // setPostCode()

    /**
     * Set the value of [adtown_lat] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adtown_lat !== $v) {
            $this->adtown_lat = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_LAT] = true;
        }

        return $this;
    } // setLat()

    /**
     * Set the value of [adtown_lon] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setLon($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adtown_lon !== $v) {
            $this->adtown_lon = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_LON] = true;
        }

        return $this;
    } // setLon()

    /**
     * Set the value of [adtown_city] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_city !== $v) {
            $this->adtown_city = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_CITY] = true;
        }

        return $this;
    } // setCity()

    /**
     * Set the value of [adtown_departement] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setDepartement($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_departement !== $v) {
            $this->adtown_departement = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_DEPARTEMENT] = true;
        }

        return $this;
    } // setDepartement()

    /**
     * Set the value of [adtown_region] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setRegion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adtown_region !== $v) {
            $this->adtown_region = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_REGION] = true;
        }

        return $this;
    } // setRegion()

    /**
     * Set the value of [adtown_pop] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setPop($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adtown_pop !== $v) {
            $this->adtown_pop = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_POP] = true;
        }

        return $this;
    } // setPop()

    /**
     * Set the value of [adtown_adm_weight] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setAdmWeight($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adtown_adm_weight !== $v) {
            $this->adtown_adm_weight = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_ADM_WEIGHT] = true;
        }

        return $this;
    } // setAdmWeight()

    /**
     * Set the value of [adtown_importance] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setImportance($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adtown_importance !== $v) {
            $this->adtown_importance = $v;
            $this->modifiedColumns[TownTableMap::COL_ADTOWN_IMPORTANCE] = true;
        }

        return $this;
    } // setImportance()

    /**
     * Sets the value of [adtown_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->adtown_created_at !== null || $dt !== null) {
            if ($this->adtown_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->adtown_created_at->format("Y-m-d H:i:s.u")) {
                $this->adtown_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[TownTableMap::COL_ADTOWN_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [adtown_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->adtown_updated_at !== null || $dt !== null) {
            if ($this->adtown_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->adtown_updated_at->format("Y-m-d H:i:s.u")) {
                $this->adtown_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[TownTableMap::COL_ADTOWN_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : TownTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : TownTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : TownTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : TownTableMap::translateFieldName('PostCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_postcode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : TownTableMap::translateFieldName('Lat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_lat = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : TownTableMap::translateFieldName('Lon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_lon = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : TownTableMap::translateFieldName('City', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : TownTableMap::translateFieldName('Departement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_departement = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : TownTableMap::translateFieldName('Region', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_region = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : TownTableMap::translateFieldName('Pop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_pop = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : TownTableMap::translateFieldName('AdmWeight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_adm_weight = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : TownTableMap::translateFieldName('Importance', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adtown_importance = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : TownTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->adtown_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : TownTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->adtown_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = TownTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\AddressBundle\\Model\\Town'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TownTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildTownQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collStreets = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Town::setDeleted()
     * @see Town::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildTownQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(TownTableMap::COL_ADTOWN_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(TownTableMap::COL_ADTOWN_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(TownTableMap::COL_ADTOWN_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TownTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->streetsScheduledForDeletion !== null) {
                if (!$this->streetsScheduledForDeletion->isEmpty()) {
                    \IiMedias\AddressBundle\Model\StreetQuery::create()
                        ->filterByPrimaryKeys($this->streetsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->streetsScheduledForDeletion = null;
                }
            }

            if ($this->collStreets !== null) {
                foreach ($this->collStreets as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_id';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_type';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_name';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_POSTCODE)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_postcode';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_LAT)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_lat';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_LON)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_lon';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_city';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_DEPARTEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_departement';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_REGION)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_region';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_POP)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_pop';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_ADM_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_adm_weight';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_IMPORTANCE)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_importance';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_created_at';
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'adtown_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO address_town_adtown (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'adtown_id':
                        $stmt->bindValue($identifier, $this->adtown_id, PDO::PARAM_INT);
                        break;
                    case 'adtown_type':
                        $stmt->bindValue($identifier, $this->adtown_type, PDO::PARAM_STR);
                        break;
                    case 'adtown_name':
                        $stmt->bindValue($identifier, $this->adtown_name, PDO::PARAM_STR);
                        break;
                    case 'adtown_postcode':
                        $stmt->bindValue($identifier, $this->adtown_postcode, PDO::PARAM_STR);
                        break;
                    case 'adtown_lat':
                        $stmt->bindValue($identifier, $this->adtown_lat, PDO::PARAM_STR);
                        break;
                    case 'adtown_lon':
                        $stmt->bindValue($identifier, $this->adtown_lon, PDO::PARAM_STR);
                        break;
                    case 'adtown_city':
                        $stmt->bindValue($identifier, $this->adtown_city, PDO::PARAM_STR);
                        break;
                    case 'adtown_departement':
                        $stmt->bindValue($identifier, $this->adtown_departement, PDO::PARAM_STR);
                        break;
                    case 'adtown_region':
                        $stmt->bindValue($identifier, $this->adtown_region, PDO::PARAM_STR);
                        break;
                    case 'adtown_pop':
                        $stmt->bindValue($identifier, $this->adtown_pop, PDO::PARAM_STR);
                        break;
                    case 'adtown_adm_weight':
                        $stmt->bindValue($identifier, $this->adtown_adm_weight, PDO::PARAM_STR);
                        break;
                    case 'adtown_importance':
                        $stmt->bindValue($identifier, $this->adtown_importance, PDO::PARAM_STR);
                        break;
                    case 'adtown_created_at':
                        $stmt->bindValue($identifier, $this->adtown_created_at ? $this->adtown_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'adtown_updated_at':
                        $stmt->bindValue($identifier, $this->adtown_updated_at ? $this->adtown_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TownTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getType();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getPostCode();
                break;
            case 4:
                return $this->getLat();
                break;
            case 5:
                return $this->getLon();
                break;
            case 6:
                return $this->getCity();
                break;
            case 7:
                return $this->getDepartement();
                break;
            case 8:
                return $this->getRegion();
                break;
            case 9:
                return $this->getPop();
                break;
            case 10:
                return $this->getAdmWeight();
                break;
            case 11:
                return $this->getImportance();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Town'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Town'][$this->hashCode()] = true;
        $keys = TownTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getType(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getPostCode(),
            $keys[4] => $this->getLat(),
            $keys[5] => $this->getLon(),
            $keys[6] => $this->getCity(),
            $keys[7] => $this->getDepartement(),
            $keys[8] => $this->getRegion(),
            $keys[9] => $this->getPop(),
            $keys[10] => $this->getAdmWeight(),
            $keys[11] => $this->getImportance(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collStreets) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'streets';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_street_adstrts';
                        break;
                    default:
                        $key = 'Streets';
                }

                $result[$key] = $this->collStreets->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\AddressBundle\Model\Town
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TownTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\AddressBundle\Model\Town
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setType($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setPostCode($value);
                break;
            case 4:
                $this->setLat($value);
                break;
            case 5:
                $this->setLon($value);
                break;
            case 6:
                $this->setCity($value);
                break;
            case 7:
                $this->setDepartement($value);
                break;
            case 8:
                $this->setRegion($value);
                break;
            case 9:
                $this->setPop($value);
                break;
            case 10:
                $this->setAdmWeight($value);
                break;
            case 11:
                $this->setImportance($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = TownTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setType($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPostCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLat($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLon($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCity($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDepartement($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setRegion($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPop($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setAdmWeight($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setImportance($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TownTableMap::DATABASE_NAME);

        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_ID)) {
            $criteria->add(TownTableMap::COL_ADTOWN_ID, $this->adtown_id);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_TYPE)) {
            $criteria->add(TownTableMap::COL_ADTOWN_TYPE, $this->adtown_type);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_NAME)) {
            $criteria->add(TownTableMap::COL_ADTOWN_NAME, $this->adtown_name);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_POSTCODE)) {
            $criteria->add(TownTableMap::COL_ADTOWN_POSTCODE, $this->adtown_postcode);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_LAT)) {
            $criteria->add(TownTableMap::COL_ADTOWN_LAT, $this->adtown_lat);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_LON)) {
            $criteria->add(TownTableMap::COL_ADTOWN_LON, $this->adtown_lon);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_CITY)) {
            $criteria->add(TownTableMap::COL_ADTOWN_CITY, $this->adtown_city);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_DEPARTEMENT)) {
            $criteria->add(TownTableMap::COL_ADTOWN_DEPARTEMENT, $this->adtown_departement);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_REGION)) {
            $criteria->add(TownTableMap::COL_ADTOWN_REGION, $this->adtown_region);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_POP)) {
            $criteria->add(TownTableMap::COL_ADTOWN_POP, $this->adtown_pop);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_ADM_WEIGHT)) {
            $criteria->add(TownTableMap::COL_ADTOWN_ADM_WEIGHT, $this->adtown_adm_weight);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_IMPORTANCE)) {
            $criteria->add(TownTableMap::COL_ADTOWN_IMPORTANCE, $this->adtown_importance);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_CREATED_AT)) {
            $criteria->add(TownTableMap::COL_ADTOWN_CREATED_AT, $this->adtown_created_at);
        }
        if ($this->isColumnModified(TownTableMap::COL_ADTOWN_UPDATED_AT)) {
            $criteria->add(TownTableMap::COL_ADTOWN_UPDATED_AT, $this->adtown_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildTownQuery::create();
        $criteria->add(TownTableMap::COL_ADTOWN_ID, $this->adtown_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (adtown_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\AddressBundle\Model\Town (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setType($this->getType());
        $copyObj->setName($this->getName());
        $copyObj->setPostCode($this->getPostCode());
        $copyObj->setLat($this->getLat());
        $copyObj->setLon($this->getLon());
        $copyObj->setCity($this->getCity());
        $copyObj->setDepartement($this->getDepartement());
        $copyObj->setRegion($this->getRegion());
        $copyObj->setPop($this->getPop());
        $copyObj->setAdmWeight($this->getAdmWeight());
        $copyObj->setImportance($this->getImportance());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getStreets() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addStreet($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\AddressBundle\Model\Town Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Street' == $relationName) {
            return $this->initStreets();
        }
    }

    /**
     * Clears out the collStreets collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addStreets()
     */
    public function clearStreets()
    {
        $this->collStreets = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collStreets collection loaded partially.
     */
    public function resetPartialStreets($v = true)
    {
        $this->collStreetsPartial = $v;
    }

    /**
     * Initializes the collStreets collection.
     *
     * By default this just sets the collStreets collection to an empty array (like clearcollStreets());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initStreets($overrideExisting = true)
    {
        if (null !== $this->collStreets && !$overrideExisting) {
            return;
        }

        $collectionClassName = StreetTableMap::getTableMap()->getCollectionClassName();

        $this->collStreets = new $collectionClassName;
        $this->collStreets->setModel('\IiMedias\AddressBundle\Model\Street');
    }

    /**
     * Gets an array of ChildStreet objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTown is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildStreet[] List of ChildStreet objects
     * @throws PropelException
     */
    public function getStreets(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collStreetsPartial && !$this->isNew();
        if (null === $this->collStreets || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collStreets) {
                // return empty collection
                $this->initStreets();
            } else {
                $collStreets = ChildStreetQuery::create(null, $criteria)
                    ->filterByTown($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collStreetsPartial && count($collStreets)) {
                        $this->initStreets(false);

                        foreach ($collStreets as $obj) {
                            if (false == $this->collStreets->contains($obj)) {
                                $this->collStreets->append($obj);
                            }
                        }

                        $this->collStreetsPartial = true;
                    }

                    return $collStreets;
                }

                if ($partial && $this->collStreets) {
                    foreach ($this->collStreets as $obj) {
                        if ($obj->isNew()) {
                            $collStreets[] = $obj;
                        }
                    }
                }

                $this->collStreets = $collStreets;
                $this->collStreetsPartial = false;
            }
        }

        return $this->collStreets;
    }

    /**
     * Sets a collection of ChildStreet objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $streets A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTown The current object (for fluent API support)
     */
    public function setStreets(Collection $streets, ConnectionInterface $con = null)
    {
        /** @var ChildStreet[] $streetsToDelete */
        $streetsToDelete = $this->getStreets(new Criteria(), $con)->diff($streets);


        $this->streetsScheduledForDeletion = $streetsToDelete;

        foreach ($streetsToDelete as $streetRemoved) {
            $streetRemoved->setTown(null);
        }

        $this->collStreets = null;
        foreach ($streets as $street) {
            $this->addStreet($street);
        }

        $this->collStreets = $streets;
        $this->collStreetsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Street objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Street objects.
     * @throws PropelException
     */
    public function countStreets(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collStreetsPartial && !$this->isNew();
        if (null === $this->collStreets || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collStreets) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getStreets());
            }

            $query = ChildStreetQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTown($this)
                ->count($con);
        }

        return count($this->collStreets);
    }

    /**
     * Method called to associate a ChildStreet object to this object
     * through the ChildStreet foreign key attribute.
     *
     * @param  ChildStreet $l ChildStreet
     * @return $this|\IiMedias\AddressBundle\Model\Town The current object (for fluent API support)
     */
    public function addStreet(ChildStreet $l)
    {
        if ($this->collStreets === null) {
            $this->initStreets();
            $this->collStreetsPartial = true;
        }

        if (!$this->collStreets->contains($l)) {
            $this->doAddStreet($l);

            if ($this->streetsScheduledForDeletion and $this->streetsScheduledForDeletion->contains($l)) {
                $this->streetsScheduledForDeletion->remove($this->streetsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildStreet $street The ChildStreet object to add.
     */
    protected function doAddStreet(ChildStreet $street)
    {
        $this->collStreets[]= $street;
        $street->setTown($this);
    }

    /**
     * @param  ChildStreet $street The ChildStreet object to remove.
     * @return $this|ChildTown The current object (for fluent API support)
     */
    public function removeStreet(ChildStreet $street)
    {
        if ($this->getStreets()->contains($street)) {
            $pos = $this->collStreets->search($street);
            $this->collStreets->remove($pos);
            if (null === $this->streetsScheduledForDeletion) {
                $this->streetsScheduledForDeletion = clone $this->collStreets;
                $this->streetsScheduledForDeletion->clear();
            }
            $this->streetsScheduledForDeletion[]= clone $street;
            $street->setTown(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->adtown_id = null;
        $this->adtown_type = null;
        $this->adtown_name = null;
        $this->adtown_postcode = null;
        $this->adtown_lat = null;
        $this->adtown_lon = null;
        $this->adtown_city = null;
        $this->adtown_departement = null;
        $this->adtown_region = null;
        $this->adtown_pop = null;
        $this->adtown_adm_weight = null;
        $this->adtown_importance = null;
        $this->adtown_created_at = null;
        $this->adtown_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collStreets) {
                foreach ($this->collStreets as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collStreets = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TownTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildTown The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[TownTableMap::COL_ADTOWN_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
