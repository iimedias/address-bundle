<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\Street as ChildStreet;
use IiMedias\AddressBundle\Model\StreetQuery as ChildStreetQuery;
use IiMedias\AddressBundle\Model\Map\StreetTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'address_street_adstrt' table.
 *
 *
 *
 * @method     ChildStreetQuery orderById($order = Criteria::ASC) Order by the adstrt_id column
 * @method     ChildStreetQuery orderByTypeId($order = Criteria::ASC) Order by the adstrt_adtype_id column
 * @method     ChildStreetQuery orderByCityId($order = Criteria::ASC) Order by the adstrt_adcity_id column
 * @method     ChildStreetQuery orderByName($order = Criteria::ASC) Order by the adstrt_name column
 * @method     ChildStreetQuery orderByPostCode($order = Criteria::ASC) Order by the adstrt_postcode column
 * @method     ChildStreetQuery orderByLat($order = Criteria::ASC) Order by the adstrt_lat column
 * @method     ChildStreetQuery orderByLon($order = Criteria::ASC) Order by the adstrt_lon column
 * @method     ChildStreetQuery orderByImportance($order = Criteria::ASC) Order by the adstrt_importance column
 * @method     ChildStreetQuery orderByHouseNumbersArray($order = Criteria::ASC) Order by the adstrt_housenumbers_array column
 *
 * @method     ChildStreetQuery groupById() Group by the adstrt_id column
 * @method     ChildStreetQuery groupByTypeId() Group by the adstrt_adtype_id column
 * @method     ChildStreetQuery groupByCityId() Group by the adstrt_adcity_id column
 * @method     ChildStreetQuery groupByName() Group by the adstrt_name column
 * @method     ChildStreetQuery groupByPostCode() Group by the adstrt_postcode column
 * @method     ChildStreetQuery groupByLat() Group by the adstrt_lat column
 * @method     ChildStreetQuery groupByLon() Group by the adstrt_lon column
 * @method     ChildStreetQuery groupByImportance() Group by the adstrt_importance column
 * @method     ChildStreetQuery groupByHouseNumbersArray() Group by the adstrt_housenumbers_array column
 *
 * @method     ChildStreetQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStreetQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStreetQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStreetQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStreetQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStreetQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStreetQuery leftJoinType($relationAlias = null) Adds a LEFT JOIN clause to the query using the Type relation
 * @method     ChildStreetQuery rightJoinType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Type relation
 * @method     ChildStreetQuery innerJoinType($relationAlias = null) Adds a INNER JOIN clause to the query using the Type relation
 *
 * @method     ChildStreetQuery joinWithType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Type relation
 *
 * @method     ChildStreetQuery leftJoinWithType() Adds a LEFT JOIN clause and with to the query using the Type relation
 * @method     ChildStreetQuery rightJoinWithType() Adds a RIGHT JOIN clause and with to the query using the Type relation
 * @method     ChildStreetQuery innerJoinWithType() Adds a INNER JOIN clause and with to the query using the Type relation
 *
 * @method     ChildStreetQuery leftJoinCity($relationAlias = null) Adds a LEFT JOIN clause to the query using the City relation
 * @method     ChildStreetQuery rightJoinCity($relationAlias = null) Adds a RIGHT JOIN clause to the query using the City relation
 * @method     ChildStreetQuery innerJoinCity($relationAlias = null) Adds a INNER JOIN clause to the query using the City relation
 *
 * @method     ChildStreetQuery joinWithCity($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the City relation
 *
 * @method     ChildStreetQuery leftJoinWithCity() Adds a LEFT JOIN clause and with to the query using the City relation
 * @method     ChildStreetQuery rightJoinWithCity() Adds a RIGHT JOIN clause and with to the query using the City relation
 * @method     ChildStreetQuery innerJoinWithCity() Adds a INNER JOIN clause and with to the query using the City relation
 *
 * @method     ChildStreetQuery leftJoinHouseNumber($relationAlias = null) Adds a LEFT JOIN clause to the query using the HouseNumber relation
 * @method     ChildStreetQuery rightJoinHouseNumber($relationAlias = null) Adds a RIGHT JOIN clause to the query using the HouseNumber relation
 * @method     ChildStreetQuery innerJoinHouseNumber($relationAlias = null) Adds a INNER JOIN clause to the query using the HouseNumber relation
 *
 * @method     ChildStreetQuery joinWithHouseNumber($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the HouseNumber relation
 *
 * @method     ChildStreetQuery leftJoinWithHouseNumber() Adds a LEFT JOIN clause and with to the query using the HouseNumber relation
 * @method     ChildStreetQuery rightJoinWithHouseNumber() Adds a RIGHT JOIN clause and with to the query using the HouseNumber relation
 * @method     ChildStreetQuery innerJoinWithHouseNumber() Adds a INNER JOIN clause and with to the query using the HouseNumber relation
 *
 * @method     \IiMedias\AddressBundle\Model\TypeQuery|\IiMedias\AddressBundle\Model\CityQuery|\IiMedias\AddressBundle\Model\HouseNumberQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStreet findOne(ConnectionInterface $con = null) Return the first ChildStreet matching the query
 * @method     ChildStreet findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStreet matching the query, or a new ChildStreet object populated from the query conditions when no match is found
 *
 * @method     ChildStreet findOneById(string $adstrt_id) Return the first ChildStreet filtered by the adstrt_id column
 * @method     ChildStreet findOneByTypeId(int $adstrt_adtype_id) Return the first ChildStreet filtered by the adstrt_adtype_id column
 * @method     ChildStreet findOneByCityId(string $adstrt_adcity_id) Return the first ChildStreet filtered by the adstrt_adcity_id column
 * @method     ChildStreet findOneByName(string $adstrt_name) Return the first ChildStreet filtered by the adstrt_name column
 * @method     ChildStreet findOneByPostCode(string $adstrt_postcode) Return the first ChildStreet filtered by the adstrt_postcode column
 * @method     ChildStreet findOneByLat(double $adstrt_lat) Return the first ChildStreet filtered by the adstrt_lat column
 * @method     ChildStreet findOneByLon(double $adstrt_lon) Return the first ChildStreet filtered by the adstrt_lon column
 * @method     ChildStreet findOneByImportance(double $adstrt_importance) Return the first ChildStreet filtered by the adstrt_importance column
 * @method     ChildStreet findOneByHouseNumbersArray(string $adstrt_housenumbers_array) Return the first ChildStreet filtered by the adstrt_housenumbers_array column *

 * @method     ChildStreet requirePk($key, ConnectionInterface $con = null) Return the ChildStreet by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOne(ConnectionInterface $con = null) Return the first ChildStreet matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStreet requireOneById(string $adstrt_id) Return the first ChildStreet filtered by the adstrt_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByTypeId(int $adstrt_adtype_id) Return the first ChildStreet filtered by the adstrt_adtype_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByCityId(string $adstrt_adcity_id) Return the first ChildStreet filtered by the adstrt_adcity_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByName(string $adstrt_name) Return the first ChildStreet filtered by the adstrt_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByPostCode(string $adstrt_postcode) Return the first ChildStreet filtered by the adstrt_postcode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByLat(double $adstrt_lat) Return the first ChildStreet filtered by the adstrt_lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByLon(double $adstrt_lon) Return the first ChildStreet filtered by the adstrt_lon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByImportance(double $adstrt_importance) Return the first ChildStreet filtered by the adstrt_importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreet requireOneByHouseNumbersArray(string $adstrt_housenumbers_array) Return the first ChildStreet filtered by the adstrt_housenumbers_array column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStreet[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStreet objects based on current ModelCriteria
 * @method     ChildStreet[]|ObjectCollection findById(string $adstrt_id) Return ChildStreet objects filtered by the adstrt_id column
 * @method     ChildStreet[]|ObjectCollection findByTypeId(int $adstrt_adtype_id) Return ChildStreet objects filtered by the adstrt_adtype_id column
 * @method     ChildStreet[]|ObjectCollection findByCityId(string $adstrt_adcity_id) Return ChildStreet objects filtered by the adstrt_adcity_id column
 * @method     ChildStreet[]|ObjectCollection findByName(string $adstrt_name) Return ChildStreet objects filtered by the adstrt_name column
 * @method     ChildStreet[]|ObjectCollection findByPostCode(string $adstrt_postcode) Return ChildStreet objects filtered by the adstrt_postcode column
 * @method     ChildStreet[]|ObjectCollection findByLat(double $adstrt_lat) Return ChildStreet objects filtered by the adstrt_lat column
 * @method     ChildStreet[]|ObjectCollection findByLon(double $adstrt_lon) Return ChildStreet objects filtered by the adstrt_lon column
 * @method     ChildStreet[]|ObjectCollection findByImportance(double $adstrt_importance) Return ChildStreet objects filtered by the adstrt_importance column
 * @method     ChildStreet[]|ObjectCollection findByHouseNumbersArray(string $adstrt_housenumbers_array) Return ChildStreet objects filtered by the adstrt_housenumbers_array column
 * @method     ChildStreet[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StreetQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AddressBundle\Model\Base\StreetQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AddressBundle\\Model\\Street', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStreetQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStreetQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStreetQuery) {
            return $criteria;
        }
        $query = new ChildStreetQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStreet|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StreetTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StreetTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreet A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT adstrt_id, adstrt_adtype_id, adstrt_adcity_id, adstrt_name, adstrt_postcode, adstrt_lat, adstrt_lon, adstrt_importance, adstrt_housenumbers_array FROM address_street_adstrt WHERE adstrt_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStreet $obj */
            $obj = new ChildStreet();
            $obj->hydrate($row);
            StreetTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStreet|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the adstrt_id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE adstrt_id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE adstrt_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ID, $id, $comparison);
    }

    /**
     * Filter the query on the adstrt_adtype_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId(1234); // WHERE adstrt_adtype_id = 1234
     * $query->filterByTypeId(array(12, 34)); // WHERE adstrt_adtype_id IN (12, 34)
     * $query->filterByTypeId(array('min' => 12)); // WHERE adstrt_adtype_id > 12
     * </code>
     *
     * @see       filterByType()
     *
     * @param     mixed $typeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (is_array($typeId)) {
            $useMinMax = false;
            if (isset($typeId['min'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $typeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeId['max'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $typeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the adstrt_adcity_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCityId('fooValue');   // WHERE adstrt_adcity_id = 'fooValue'
     * $query->filterByCityId('%fooValue%'); // WHERE adstrt_adcity_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cityId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByCityId($cityId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cityId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ADCITY_ID, $cityId, $comparison);
    }

    /**
     * Filter the query on the adstrt_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE adstrt_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE adstrt_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the adstrt_postcode column
     *
     * Example usage:
     * <code>
     * $query->filterByPostCode('fooValue');   // WHERE adstrt_postcode = 'fooValue'
     * $query->filterByPostCode('%fooValue%'); // WHERE adstrt_postcode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByPostCode($postCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_POSTCODE, $postCode, $comparison);
    }

    /**
     * Filter the query on the adstrt_lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE adstrt_lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE adstrt_lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE adstrt_lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the adstrt_lon column
     *
     * Example usage:
     * <code>
     * $query->filterByLon(1234); // WHERE adstrt_lon = 1234
     * $query->filterByLon(array(12, 34)); // WHERE adstrt_lon IN (12, 34)
     * $query->filterByLon(array('min' => 12)); // WHERE adstrt_lon > 12
     * </code>
     *
     * @param     mixed $lon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByLon($lon = null, $comparison = null)
    {
        if (is_array($lon)) {
            $useMinMax = false;
            if (isset($lon['min'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LON, $lon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lon['max'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LON, $lon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_LON, $lon, $comparison);
    }

    /**
     * Filter the query on the adstrt_importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE adstrt_importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE adstrt_importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE adstrt_importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(StreetTableMap::COL_ADSTRT_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the adstrt_housenumbers_array column
     *
     * Example usage:
     * <code>
     * $query->filterByHouseNumbersArray('fooValue');   // WHERE adstrt_housenumbers_array = 'fooValue'
     * $query->filterByHouseNumbersArray('%fooValue%'); // WHERE adstrt_housenumbers_array LIKE '%fooValue%'
     * </code>
     *
     * @param     string $houseNumbersArray The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function filterByHouseNumbersArray($houseNumbersArray = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($houseNumbersArray)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY, $houseNumbersArray, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\Type object
     *
     * @param \IiMedias\AddressBundle\Model\Type|ObjectCollection $type The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreetQuery The current query, for fluid interface
     */
    public function filterByType($type, $comparison = null)
    {
        if ($type instanceof \IiMedias\AddressBundle\Model\Type) {
            return $this
                ->addUsingAlias(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $type->getId(), $comparison);
        } elseif ($type instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $type->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByType() only accepts arguments of type \IiMedias\AddressBundle\Model\Type or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Type relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function joinType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Type');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Type');
        }

        return $this;
    }

    /**
     * Use the Type relation Type object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\TypeQuery A secondary query class using the current class as primary query
     */
    public function useTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Type', '\IiMedias\AddressBundle\Model\TypeQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\City object
     *
     * @param \IiMedias\AddressBundle\Model\City|ObjectCollection $city The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreetQuery The current query, for fluid interface
     */
    public function filterByCity($city, $comparison = null)
    {
        if ($city instanceof \IiMedias\AddressBundle\Model\City) {
            return $this
                ->addUsingAlias(StreetTableMap::COL_ADSTRT_ADCITY_ID, $city->getId(), $comparison);
        } elseif ($city instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StreetTableMap::COL_ADSTRT_ADCITY_ID, $city->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCity() only accepts arguments of type \IiMedias\AddressBundle\Model\City or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the City relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function joinCity($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('City');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'City');
        }

        return $this;
    }

    /**
     * Use the City relation City object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\CityQuery A secondary query class using the current class as primary query
     */
    public function useCityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCity($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'City', '\IiMedias\AddressBundle\Model\CityQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\HouseNumber object
     *
     * @param \IiMedias\AddressBundle\Model\HouseNumber|ObjectCollection $houseNumber the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreetQuery The current query, for fluid interface
     */
    public function filterByHouseNumber($houseNumber, $comparison = null)
    {
        if ($houseNumber instanceof \IiMedias\AddressBundle\Model\HouseNumber) {
            return $this
                ->addUsingAlias(StreetTableMap::COL_ADSTRT_ID, $houseNumber->getStreetId(), $comparison);
        } elseif ($houseNumber instanceof ObjectCollection) {
            return $this
                ->useHouseNumberQuery()
                ->filterByPrimaryKeys($houseNumber->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByHouseNumber() only accepts arguments of type \IiMedias\AddressBundle\Model\HouseNumber or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the HouseNumber relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function joinHouseNumber($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('HouseNumber');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'HouseNumber');
        }

        return $this;
    }

    /**
     * Use the HouseNumber relation HouseNumber object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\HouseNumberQuery A secondary query class using the current class as primary query
     */
    public function useHouseNumberQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinHouseNumber($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'HouseNumber', '\IiMedias\AddressBundle\Model\HouseNumberQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStreet $street Object to remove from the list of results
     *
     * @return $this|ChildStreetQuery The current query, for fluid interface
     */
    public function prune($street = null)
    {
        if ($street) {
            $this->addUsingAlias(StreetTableMap::COL_ADSTRT_ID, $street->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address_street_adstrt table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StreetTableMap::clearInstancePool();
            StreetTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StreetTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StreetTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StreetTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StreetQuery
