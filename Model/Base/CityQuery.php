<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\City as ChildCity;
use IiMedias\AddressBundle\Model\CityQuery as ChildCityQuery;
use IiMedias\AddressBundle\Model\Map\CityTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'address_city_adcity' table.
 *
 *
 *
 * @method     ChildCityQuery orderById($order = Criteria::ASC) Order by the adcity_id column
 * @method     ChildCityQuery orderByTypeId($order = Criteria::ASC) Order by the adcity_adtype_id column
 * @method     ChildCityQuery orderByCountyId($order = Criteria::ASC) Order by the adcity_adcnty_id column
 * @method     ChildCityQuery orderByName($order = Criteria::ASC) Order by the adcity_name column
 * @method     ChildCityQuery orderByPostCode($order = Criteria::ASC) Order by the adcity_postcode column
 * @method     ChildCityQuery orderByAlternativePostCode1($order = Criteria::ASC) Order by the adcity_alternative_postcode_1 column
 * @method     ChildCityQuery orderByAlternativePostCode2($order = Criteria::ASC) Order by the adcity_alternative_postcode_2 column
 * @method     ChildCityQuery orderByAlternativePostCode3($order = Criteria::ASC) Order by the adcity_alternative_postcode_3 column
 * @method     ChildCityQuery orderByLat($order = Criteria::ASC) Order by the adcity_lat column
 * @method     ChildCityQuery orderByLon($order = Criteria::ASC) Order by the adcity_lon column
 * @method     ChildCityQuery orderByPop($order = Criteria::ASC) Order by the adcity_pop column
 * @method     ChildCityQuery orderByAdmWeight($order = Criteria::ASC) Order by the adcity_adm_weight column
 * @method     ChildCityQuery orderByImportance($order = Criteria::ASC) Order by the adcity_importance column
 * @method     ChildCityQuery orderByHouseNumbersJson($order = Criteria::ASC) Order by the adcity_housenumbers_json column
 *
 * @method     ChildCityQuery groupById() Group by the adcity_id column
 * @method     ChildCityQuery groupByTypeId() Group by the adcity_adtype_id column
 * @method     ChildCityQuery groupByCountyId() Group by the adcity_adcnty_id column
 * @method     ChildCityQuery groupByName() Group by the adcity_name column
 * @method     ChildCityQuery groupByPostCode() Group by the adcity_postcode column
 * @method     ChildCityQuery groupByAlternativePostCode1() Group by the adcity_alternative_postcode_1 column
 * @method     ChildCityQuery groupByAlternativePostCode2() Group by the adcity_alternative_postcode_2 column
 * @method     ChildCityQuery groupByAlternativePostCode3() Group by the adcity_alternative_postcode_3 column
 * @method     ChildCityQuery groupByLat() Group by the adcity_lat column
 * @method     ChildCityQuery groupByLon() Group by the adcity_lon column
 * @method     ChildCityQuery groupByPop() Group by the adcity_pop column
 * @method     ChildCityQuery groupByAdmWeight() Group by the adcity_adm_weight column
 * @method     ChildCityQuery groupByImportance() Group by the adcity_importance column
 * @method     ChildCityQuery groupByHouseNumbersJson() Group by the adcity_housenumbers_json column
 *
 * @method     ChildCityQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCityQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCityQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCityQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCityQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCityQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCityQuery leftJoinType($relationAlias = null) Adds a LEFT JOIN clause to the query using the Type relation
 * @method     ChildCityQuery rightJoinType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Type relation
 * @method     ChildCityQuery innerJoinType($relationAlias = null) Adds a INNER JOIN clause to the query using the Type relation
 *
 * @method     ChildCityQuery joinWithType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Type relation
 *
 * @method     ChildCityQuery leftJoinWithType() Adds a LEFT JOIN clause and with to the query using the Type relation
 * @method     ChildCityQuery rightJoinWithType() Adds a RIGHT JOIN clause and with to the query using the Type relation
 * @method     ChildCityQuery innerJoinWithType() Adds a INNER JOIN clause and with to the query using the Type relation
 *
 * @method     ChildCityQuery leftJoinCounty($relationAlias = null) Adds a LEFT JOIN clause to the query using the County relation
 * @method     ChildCityQuery rightJoinCounty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the County relation
 * @method     ChildCityQuery innerJoinCounty($relationAlias = null) Adds a INNER JOIN clause to the query using the County relation
 *
 * @method     ChildCityQuery joinWithCounty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the County relation
 *
 * @method     ChildCityQuery leftJoinWithCounty() Adds a LEFT JOIN clause and with to the query using the County relation
 * @method     ChildCityQuery rightJoinWithCounty() Adds a RIGHT JOIN clause and with to the query using the County relation
 * @method     ChildCityQuery innerJoinWithCounty() Adds a INNER JOIN clause and with to the query using the County relation
 *
 * @method     ChildCityQuery leftJoinStreet($relationAlias = null) Adds a LEFT JOIN clause to the query using the Street relation
 * @method     ChildCityQuery rightJoinStreet($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Street relation
 * @method     ChildCityQuery innerJoinStreet($relationAlias = null) Adds a INNER JOIN clause to the query using the Street relation
 *
 * @method     ChildCityQuery joinWithStreet($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Street relation
 *
 * @method     ChildCityQuery leftJoinWithStreet() Adds a LEFT JOIN clause and with to the query using the Street relation
 * @method     ChildCityQuery rightJoinWithStreet() Adds a RIGHT JOIN clause and with to the query using the Street relation
 * @method     ChildCityQuery innerJoinWithStreet() Adds a INNER JOIN clause and with to the query using the Street relation
 *
 * @method     \IiMedias\AddressBundle\Model\TypeQuery|\IiMedias\AddressBundle\Model\CountyQuery|\IiMedias\AddressBundle\Model\StreetQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCity findOne(ConnectionInterface $con = null) Return the first ChildCity matching the query
 * @method     ChildCity findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCity matching the query, or a new ChildCity object populated from the query conditions when no match is found
 *
 * @method     ChildCity findOneById(string $adcity_id) Return the first ChildCity filtered by the adcity_id column
 * @method     ChildCity findOneByTypeId(int $adcity_adtype_id) Return the first ChildCity filtered by the adcity_adtype_id column
 * @method     ChildCity findOneByCountyId(int $adcity_adcnty_id) Return the first ChildCity filtered by the adcity_adcnty_id column
 * @method     ChildCity findOneByName(string $adcity_name) Return the first ChildCity filtered by the adcity_name column
 * @method     ChildCity findOneByPostCode(string $adcity_postcode) Return the first ChildCity filtered by the adcity_postcode column
 * @method     ChildCity findOneByAlternativePostCode1(string $adcity_alternative_postcode_1) Return the first ChildCity filtered by the adcity_alternative_postcode_1 column
 * @method     ChildCity findOneByAlternativePostCode2(string $adcity_alternative_postcode_2) Return the first ChildCity filtered by the adcity_alternative_postcode_2 column
 * @method     ChildCity findOneByAlternativePostCode3(string $adcity_alternative_postcode_3) Return the first ChildCity filtered by the adcity_alternative_postcode_3 column
 * @method     ChildCity findOneByLat(double $adcity_lat) Return the first ChildCity filtered by the adcity_lat column
 * @method     ChildCity findOneByLon(double $adcity_lon) Return the first ChildCity filtered by the adcity_lon column
 * @method     ChildCity findOneByPop(double $adcity_pop) Return the first ChildCity filtered by the adcity_pop column
 * @method     ChildCity findOneByAdmWeight(double $adcity_adm_weight) Return the first ChildCity filtered by the adcity_adm_weight column
 * @method     ChildCity findOneByImportance(double $adcity_importance) Return the first ChildCity filtered by the adcity_importance column
 * @method     ChildCity findOneByHouseNumbersJson(string $adcity_housenumbers_json) Return the first ChildCity filtered by the adcity_housenumbers_json column *

 * @method     ChildCity requirePk($key, ConnectionInterface $con = null) Return the ChildCity by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOne(ConnectionInterface $con = null) Return the first ChildCity matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCity requireOneById(string $adcity_id) Return the first ChildCity filtered by the adcity_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByTypeId(int $adcity_adtype_id) Return the first ChildCity filtered by the adcity_adtype_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByCountyId(int $adcity_adcnty_id) Return the first ChildCity filtered by the adcity_adcnty_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByName(string $adcity_name) Return the first ChildCity filtered by the adcity_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByPostCode(string $adcity_postcode) Return the first ChildCity filtered by the adcity_postcode column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByAlternativePostCode1(string $adcity_alternative_postcode_1) Return the first ChildCity filtered by the adcity_alternative_postcode_1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByAlternativePostCode2(string $adcity_alternative_postcode_2) Return the first ChildCity filtered by the adcity_alternative_postcode_2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByAlternativePostCode3(string $adcity_alternative_postcode_3) Return the first ChildCity filtered by the adcity_alternative_postcode_3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByLat(double $adcity_lat) Return the first ChildCity filtered by the adcity_lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByLon(double $adcity_lon) Return the first ChildCity filtered by the adcity_lon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByPop(double $adcity_pop) Return the first ChildCity filtered by the adcity_pop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByAdmWeight(double $adcity_adm_weight) Return the first ChildCity filtered by the adcity_adm_weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByImportance(double $adcity_importance) Return the first ChildCity filtered by the adcity_importance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCity requireOneByHouseNumbersJson(string $adcity_housenumbers_json) Return the first ChildCity filtered by the adcity_housenumbers_json column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCity[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCity objects based on current ModelCriteria
 * @method     ChildCity[]|ObjectCollection findById(string $adcity_id) Return ChildCity objects filtered by the adcity_id column
 * @method     ChildCity[]|ObjectCollection findByTypeId(int $adcity_adtype_id) Return ChildCity objects filtered by the adcity_adtype_id column
 * @method     ChildCity[]|ObjectCollection findByCountyId(int $adcity_adcnty_id) Return ChildCity objects filtered by the adcity_adcnty_id column
 * @method     ChildCity[]|ObjectCollection findByName(string $adcity_name) Return ChildCity objects filtered by the adcity_name column
 * @method     ChildCity[]|ObjectCollection findByPostCode(string $adcity_postcode) Return ChildCity objects filtered by the adcity_postcode column
 * @method     ChildCity[]|ObjectCollection findByAlternativePostCode1(string $adcity_alternative_postcode_1) Return ChildCity objects filtered by the adcity_alternative_postcode_1 column
 * @method     ChildCity[]|ObjectCollection findByAlternativePostCode2(string $adcity_alternative_postcode_2) Return ChildCity objects filtered by the adcity_alternative_postcode_2 column
 * @method     ChildCity[]|ObjectCollection findByAlternativePostCode3(string $adcity_alternative_postcode_3) Return ChildCity objects filtered by the adcity_alternative_postcode_3 column
 * @method     ChildCity[]|ObjectCollection findByLat(double $adcity_lat) Return ChildCity objects filtered by the adcity_lat column
 * @method     ChildCity[]|ObjectCollection findByLon(double $adcity_lon) Return ChildCity objects filtered by the adcity_lon column
 * @method     ChildCity[]|ObjectCollection findByPop(double $adcity_pop) Return ChildCity objects filtered by the adcity_pop column
 * @method     ChildCity[]|ObjectCollection findByAdmWeight(double $adcity_adm_weight) Return ChildCity objects filtered by the adcity_adm_weight column
 * @method     ChildCity[]|ObjectCollection findByImportance(double $adcity_importance) Return ChildCity objects filtered by the adcity_importance column
 * @method     ChildCity[]|ObjectCollection findByHouseNumbersJson(string $adcity_housenumbers_json) Return ChildCity objects filtered by the adcity_housenumbers_json column
 * @method     ChildCity[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CityQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AddressBundle\Model\Base\CityQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AddressBundle\\Model\\City', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCityQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCityQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCityQuery) {
            return $criteria;
        }
        $query = new ChildCityQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCity|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CityTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CityTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCity A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT adcity_id, adcity_adtype_id, adcity_adcnty_id, adcity_name, adcity_postcode, adcity_alternative_postcode_1, adcity_alternative_postcode_2, adcity_alternative_postcode_3, adcity_lat, adcity_lon, adcity_pop, adcity_adm_weight, adcity_importance, adcity_housenumbers_json FROM address_city_adcity WHERE adcity_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCity $obj */
            $obj = new ChildCity();
            $obj->hydrate($row);
            CityTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCity|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the adcity_id column
     *
     * Example usage:
     * <code>
     * $query->filterById('fooValue');   // WHERE adcity_id = 'fooValue'
     * $query->filterById('%fooValue%'); // WHERE adcity_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $id The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($id)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ID, $id, $comparison);
    }

    /**
     * Filter the query on the adcity_adtype_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId(1234); // WHERE adcity_adtype_id = 1234
     * $query->filterByTypeId(array(12, 34)); // WHERE adcity_adtype_id IN (12, 34)
     * $query->filterByTypeId(array('min' => 12)); // WHERE adcity_adtype_id > 12
     * </code>
     *
     * @see       filterByType()
     *
     * @param     mixed $typeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (is_array($typeId)) {
            $useMinMax = false;
            if (isset($typeId['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADTYPE_ID, $typeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeId['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADTYPE_ID, $typeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ADTYPE_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the adcity_adcnty_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCountyId(1234); // WHERE adcity_adcnty_id = 1234
     * $query->filterByCountyId(array(12, 34)); // WHERE adcity_adcnty_id IN (12, 34)
     * $query->filterByCountyId(array('min' => 12)); // WHERE adcity_adcnty_id > 12
     * </code>
     *
     * @see       filterByCounty()
     *
     * @param     mixed $countyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByCountyId($countyId = null, $comparison = null)
    {
        if (is_array($countyId)) {
            $useMinMax = false;
            if (isset($countyId['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADCNTY_ID, $countyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countyId['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADCNTY_ID, $countyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ADCNTY_ID, $countyId, $comparison);
    }

    /**
     * Filter the query on the adcity_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE adcity_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE adcity_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the adcity_postcode column
     *
     * Example usage:
     * <code>
     * $query->filterByPostCode('fooValue');   // WHERE adcity_postcode = 'fooValue'
     * $query->filterByPostCode('%fooValue%'); // WHERE adcity_postcode LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postCode The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByPostCode($postCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_POSTCODE, $postCode, $comparison);
    }

    /**
     * Filter the query on the adcity_alternative_postcode_1 column
     *
     * Example usage:
     * <code>
     * $query->filterByAlternativePostCode1('fooValue');   // WHERE adcity_alternative_postcode_1 = 'fooValue'
     * $query->filterByAlternativePostCode1('%fooValue%'); // WHERE adcity_alternative_postcode_1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alternativePostCode1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByAlternativePostCode1($alternativePostCode1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alternativePostCode1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1, $alternativePostCode1, $comparison);
    }

    /**
     * Filter the query on the adcity_alternative_postcode_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByAlternativePostCode2('fooValue');   // WHERE adcity_alternative_postcode_2 = 'fooValue'
     * $query->filterByAlternativePostCode2('%fooValue%'); // WHERE adcity_alternative_postcode_2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alternativePostCode2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByAlternativePostCode2($alternativePostCode2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alternativePostCode2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2, $alternativePostCode2, $comparison);
    }

    /**
     * Filter the query on the adcity_alternative_postcode_3 column
     *
     * Example usage:
     * <code>
     * $query->filterByAlternativePostCode3('fooValue');   // WHERE adcity_alternative_postcode_3 = 'fooValue'
     * $query->filterByAlternativePostCode3('%fooValue%'); // WHERE adcity_alternative_postcode_3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alternativePostCode3 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByAlternativePostCode3($alternativePostCode3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alternativePostCode3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3, $alternativePostCode3, $comparison);
    }

    /**
     * Filter the query on the adcity_lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE adcity_lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE adcity_lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE adcity_lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the adcity_lon column
     *
     * Example usage:
     * <code>
     * $query->filterByLon(1234); // WHERE adcity_lon = 1234
     * $query->filterByLon(array(12, 34)); // WHERE adcity_lon IN (12, 34)
     * $query->filterByLon(array('min' => 12)); // WHERE adcity_lon > 12
     * </code>
     *
     * @param     mixed $lon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByLon($lon = null, $comparison = null)
    {
        if (is_array($lon)) {
            $useMinMax = false;
            if (isset($lon['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_LON, $lon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lon['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_LON, $lon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_LON, $lon, $comparison);
    }

    /**
     * Filter the query on the adcity_pop column
     *
     * Example usage:
     * <code>
     * $query->filterByPop(1234); // WHERE adcity_pop = 1234
     * $query->filterByPop(array(12, 34)); // WHERE adcity_pop IN (12, 34)
     * $query->filterByPop(array('min' => 12)); // WHERE adcity_pop > 12
     * </code>
     *
     * @param     mixed $pop The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByPop($pop = null, $comparison = null)
    {
        if (is_array($pop)) {
            $useMinMax = false;
            if (isset($pop['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_POP, $pop['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pop['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_POP, $pop['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_POP, $pop, $comparison);
    }

    /**
     * Filter the query on the adcity_adm_weight column
     *
     * Example usage:
     * <code>
     * $query->filterByAdmWeight(1234); // WHERE adcity_adm_weight = 1234
     * $query->filterByAdmWeight(array(12, 34)); // WHERE adcity_adm_weight IN (12, 34)
     * $query->filterByAdmWeight(array('min' => 12)); // WHERE adcity_adm_weight > 12
     * </code>
     *
     * @param     mixed $admWeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByAdmWeight($admWeight = null, $comparison = null)
    {
        if (is_array($admWeight)) {
            $useMinMax = false;
            if (isset($admWeight['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADM_WEIGHT, $admWeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($admWeight['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_ADM_WEIGHT, $admWeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_ADM_WEIGHT, $admWeight, $comparison);
    }

    /**
     * Filter the query on the adcity_importance column
     *
     * Example usage:
     * <code>
     * $query->filterByImportance(1234); // WHERE adcity_importance = 1234
     * $query->filterByImportance(array(12, 34)); // WHERE adcity_importance IN (12, 34)
     * $query->filterByImportance(array('min' => 12)); // WHERE adcity_importance > 12
     * </code>
     *
     * @param     mixed $importance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByImportance($importance = null, $comparison = null)
    {
        if (is_array($importance)) {
            $useMinMax = false;
            if (isset($importance['min'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_IMPORTANCE, $importance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importance['max'])) {
                $this->addUsingAlias(CityTableMap::COL_ADCITY_IMPORTANCE, $importance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_IMPORTANCE, $importance, $comparison);
    }

    /**
     * Filter the query on the adcity_housenumbers_json column
     *
     * Example usage:
     * <code>
     * $query->filterByHouseNumbersJson('fooValue');   // WHERE adcity_housenumbers_json = 'fooValue'
     * $query->filterByHouseNumbersJson('%fooValue%'); // WHERE adcity_housenumbers_json LIKE '%fooValue%'
     * </code>
     *
     * @param     string $houseNumbersJson The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function filterByHouseNumbersJson($houseNumbersJson = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($houseNumbersJson)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON, $houseNumbersJson, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\Type object
     *
     * @param \IiMedias\AddressBundle\Model\Type|ObjectCollection $type The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCityQuery The current query, for fluid interface
     */
    public function filterByType($type, $comparison = null)
    {
        if ($type instanceof \IiMedias\AddressBundle\Model\Type) {
            return $this
                ->addUsingAlias(CityTableMap::COL_ADCITY_ADTYPE_ID, $type->getId(), $comparison);
        } elseif ($type instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CityTableMap::COL_ADCITY_ADTYPE_ID, $type->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByType() only accepts arguments of type \IiMedias\AddressBundle\Model\Type or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Type relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function joinType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Type');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Type');
        }

        return $this;
    }

    /**
     * Use the Type relation Type object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\TypeQuery A secondary query class using the current class as primary query
     */
    public function useTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Type', '\IiMedias\AddressBundle\Model\TypeQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\County object
     *
     * @param \IiMedias\AddressBundle\Model\County|ObjectCollection $county The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCityQuery The current query, for fluid interface
     */
    public function filterByCounty($county, $comparison = null)
    {
        if ($county instanceof \IiMedias\AddressBundle\Model\County) {
            return $this
                ->addUsingAlias(CityTableMap::COL_ADCITY_ADCNTY_ID, $county->getId(), $comparison);
        } elseif ($county instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CityTableMap::COL_ADCITY_ADCNTY_ID, $county->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCounty() only accepts arguments of type \IiMedias\AddressBundle\Model\County or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the County relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function joinCounty($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('County');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'County');
        }

        return $this;
    }

    /**
     * Use the County relation County object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\CountyQuery A secondary query class using the current class as primary query
     */
    public function useCountyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCounty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'County', '\IiMedias\AddressBundle\Model\CountyQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\Street object
     *
     * @param \IiMedias\AddressBundle\Model\Street|ObjectCollection $street the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCityQuery The current query, for fluid interface
     */
    public function filterByStreet($street, $comparison = null)
    {
        if ($street instanceof \IiMedias\AddressBundle\Model\Street) {
            return $this
                ->addUsingAlias(CityTableMap::COL_ADCITY_ID, $street->getCityId(), $comparison);
        } elseif ($street instanceof ObjectCollection) {
            return $this
                ->useStreetQuery()
                ->filterByPrimaryKeys($street->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByStreet() only accepts arguments of type \IiMedias\AddressBundle\Model\Street or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Street relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function joinStreet($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Street');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Street');
        }

        return $this;
    }

    /**
     * Use the Street relation Street object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\StreetQuery A secondary query class using the current class as primary query
     */
    public function useStreetQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStreet($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Street', '\IiMedias\AddressBundle\Model\StreetQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCity $city Object to remove from the list of results
     *
     * @return $this|ChildCityQuery The current query, for fluid interface
     */
    public function prune($city = null)
    {
        if ($city) {
            $this->addUsingAlias(CityTableMap::COL_ADCITY_ID, $city->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address_city_adcity table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CityTableMap::clearInstancePool();
            CityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CityTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CityTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CityQuery
