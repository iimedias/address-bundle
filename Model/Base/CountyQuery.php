<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\County as ChildCounty;
use IiMedias\AddressBundle\Model\CountyQuery as ChildCountyQuery;
use IiMedias\AddressBundle\Model\Map\CountyTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'address_county_adcnty' table.
 *
 *
 *
 * @method     ChildCountyQuery orderById($order = Criteria::ASC) Order by the adcnty_id column
 * @method     ChildCountyQuery orderByRegionId($order = Criteria::ASC) Order by the adcnty_adregn_id column
 * @method     ChildCountyQuery orderByCode($order = Criteria::ASC) Order by the adcnty_code column
 * @method     ChildCountyQuery orderByName($order = Criteria::ASC) Order by the adcnty_name column
 *
 * @method     ChildCountyQuery groupById() Group by the adcnty_id column
 * @method     ChildCountyQuery groupByRegionId() Group by the adcnty_adregn_id column
 * @method     ChildCountyQuery groupByCode() Group by the adcnty_code column
 * @method     ChildCountyQuery groupByName() Group by the adcnty_name column
 *
 * @method     ChildCountyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCountyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCountyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCountyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCountyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCountyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCountyQuery leftJoinRegion($relationAlias = null) Adds a LEFT JOIN clause to the query using the Region relation
 * @method     ChildCountyQuery rightJoinRegion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Region relation
 * @method     ChildCountyQuery innerJoinRegion($relationAlias = null) Adds a INNER JOIN clause to the query using the Region relation
 *
 * @method     ChildCountyQuery joinWithRegion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Region relation
 *
 * @method     ChildCountyQuery leftJoinWithRegion() Adds a LEFT JOIN clause and with to the query using the Region relation
 * @method     ChildCountyQuery rightJoinWithRegion() Adds a RIGHT JOIN clause and with to the query using the Region relation
 * @method     ChildCountyQuery innerJoinWithRegion() Adds a INNER JOIN clause and with to the query using the Region relation
 *
 * @method     ChildCountyQuery leftJoinCity($relationAlias = null) Adds a LEFT JOIN clause to the query using the City relation
 * @method     ChildCountyQuery rightJoinCity($relationAlias = null) Adds a RIGHT JOIN clause to the query using the City relation
 * @method     ChildCountyQuery innerJoinCity($relationAlias = null) Adds a INNER JOIN clause to the query using the City relation
 *
 * @method     ChildCountyQuery joinWithCity($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the City relation
 *
 * @method     ChildCountyQuery leftJoinWithCity() Adds a LEFT JOIN clause and with to the query using the City relation
 * @method     ChildCountyQuery rightJoinWithCity() Adds a RIGHT JOIN clause and with to the query using the City relation
 * @method     ChildCountyQuery innerJoinWithCity() Adds a INNER JOIN clause and with to the query using the City relation
 *
 * @method     \IiMedias\AddressBundle\Model\RegionQuery|\IiMedias\AddressBundle\Model\CityQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCounty findOne(ConnectionInterface $con = null) Return the first ChildCounty matching the query
 * @method     ChildCounty findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCounty matching the query, or a new ChildCounty object populated from the query conditions when no match is found
 *
 * @method     ChildCounty findOneById(int $adcnty_id) Return the first ChildCounty filtered by the adcnty_id column
 * @method     ChildCounty findOneByRegionId(int $adcnty_adregn_id) Return the first ChildCounty filtered by the adcnty_adregn_id column
 * @method     ChildCounty findOneByCode(string $adcnty_code) Return the first ChildCounty filtered by the adcnty_code column
 * @method     ChildCounty findOneByName(string $adcnty_name) Return the first ChildCounty filtered by the adcnty_name column *

 * @method     ChildCounty requirePk($key, ConnectionInterface $con = null) Return the ChildCounty by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCounty requireOne(ConnectionInterface $con = null) Return the first ChildCounty matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCounty requireOneById(int $adcnty_id) Return the first ChildCounty filtered by the adcnty_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCounty requireOneByRegionId(int $adcnty_adregn_id) Return the first ChildCounty filtered by the adcnty_adregn_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCounty requireOneByCode(string $adcnty_code) Return the first ChildCounty filtered by the adcnty_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCounty requireOneByName(string $adcnty_name) Return the first ChildCounty filtered by the adcnty_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCounty[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCounty objects based on current ModelCriteria
 * @method     ChildCounty[]|ObjectCollection findById(int $adcnty_id) Return ChildCounty objects filtered by the adcnty_id column
 * @method     ChildCounty[]|ObjectCollection findByRegionId(int $adcnty_adregn_id) Return ChildCounty objects filtered by the adcnty_adregn_id column
 * @method     ChildCounty[]|ObjectCollection findByCode(string $adcnty_code) Return ChildCounty objects filtered by the adcnty_code column
 * @method     ChildCounty[]|ObjectCollection findByName(string $adcnty_name) Return ChildCounty objects filtered by the adcnty_name column
 * @method     ChildCounty[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CountyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AddressBundle\Model\Base\CountyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AddressBundle\\Model\\County', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCountyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCountyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCountyQuery) {
            return $criteria;
        }
        $query = new ChildCountyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCounty|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CountyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CountyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCounty A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT adcnty_id, adcnty_adregn_id, adcnty_code, adcnty_name FROM address_county_adcnty WHERE adcnty_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCounty $obj */
            $obj = new ChildCounty();
            $obj->hydrate($row);
            CountyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCounty|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the adcnty_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE adcnty_id = 1234
     * $query->filterById(array(12, 34)); // WHERE adcnty_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE adcnty_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $id, $comparison);
    }

    /**
     * Filter the query on the adcnty_adregn_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRegionId(1234); // WHERE adcnty_adregn_id = 1234
     * $query->filterByRegionId(array(12, 34)); // WHERE adcnty_adregn_id IN (12, 34)
     * $query->filterByRegionId(array('min' => 12)); // WHERE adcnty_adregn_id > 12
     * </code>
     *
     * @see       filterByRegion()
     *
     * @param     mixed $regionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterByRegionId($regionId = null, $comparison = null)
    {
        if (is_array($regionId)) {
            $useMinMax = false;
            if (isset($regionId['min'])) {
                $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ADREGN_ID, $regionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regionId['max'])) {
                $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ADREGN_ID, $regionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ADREGN_ID, $regionId, $comparison);
    }

    /**
     * Filter the query on the adcnty_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE adcnty_code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE adcnty_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the adcnty_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE adcnty_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE adcnty_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountyTableMap::COL_ADCNTY_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\Region object
     *
     * @param \IiMedias\AddressBundle\Model\Region|ObjectCollection $region The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountyQuery The current query, for fluid interface
     */
    public function filterByRegion($region, $comparison = null)
    {
        if ($region instanceof \IiMedias\AddressBundle\Model\Region) {
            return $this
                ->addUsingAlias(CountyTableMap::COL_ADCNTY_ADREGN_ID, $region->getId(), $comparison);
        } elseif ($region instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CountyTableMap::COL_ADCNTY_ADREGN_ID, $region->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRegion() only accepts arguments of type \IiMedias\AddressBundle\Model\Region or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Region relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function joinRegion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Region');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Region');
        }

        return $this;
    }

    /**
     * Use the Region relation Region object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\RegionQuery A secondary query class using the current class as primary query
     */
    public function useRegionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRegion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Region', '\IiMedias\AddressBundle\Model\RegionQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AddressBundle\Model\City object
     *
     * @param \IiMedias\AddressBundle\Model\City|ObjectCollection $city the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountyQuery The current query, for fluid interface
     */
    public function filterByCity($city, $comparison = null)
    {
        if ($city instanceof \IiMedias\AddressBundle\Model\City) {
            return $this
                ->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $city->getCountyId(), $comparison);
        } elseif ($city instanceof ObjectCollection) {
            return $this
                ->useCityQuery()
                ->filterByPrimaryKeys($city->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCity() only accepts arguments of type \IiMedias\AddressBundle\Model\City or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the City relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function joinCity($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('City');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'City');
        }

        return $this;
    }

    /**
     * Use the City relation City object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AddressBundle\Model\CityQuery A secondary query class using the current class as primary query
     */
    public function useCityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCity($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'City', '\IiMedias\AddressBundle\Model\CityQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCounty $county Object to remove from the list of results
     *
     * @return $this|ChildCountyQuery The current query, for fluid interface
     */
    public function prune($county = null)
    {
        if ($county) {
            $this->addUsingAlias(CountyTableMap::COL_ADCNTY_ID, $county->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address_county_adcnty table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CountyTableMap::clearInstancePool();
            CountyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CountyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CountyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CountyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CountyQuery
