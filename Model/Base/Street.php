<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\City as ChildCity;
use IiMedias\AddressBundle\Model\CityQuery as ChildCityQuery;
use IiMedias\AddressBundle\Model\HouseNumber as ChildHouseNumber;
use IiMedias\AddressBundle\Model\HouseNumberQuery as ChildHouseNumberQuery;
use IiMedias\AddressBundle\Model\Street as ChildStreet;
use IiMedias\AddressBundle\Model\StreetQuery as ChildStreetQuery;
use IiMedias\AddressBundle\Model\Type as ChildType;
use IiMedias\AddressBundle\Model\TypeQuery as ChildTypeQuery;
use IiMedias\AddressBundle\Model\Map\HouseNumberTableMap;
use IiMedias\AddressBundle\Model\Map\StreetTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'address_street_adstrt' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.AddressBundle.Model.Base
 */
abstract class Street implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\AddressBundle\\Model\\Map\\StreetTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the adstrt_id field.
     *
     * @var        string
     */
    protected $adstrt_id;

    /**
     * The value for the adstrt_adtype_id field.
     *
     * @var        int
     */
    protected $adstrt_adtype_id;

    /**
     * The value for the adstrt_adcity_id field.
     *
     * @var        string
     */
    protected $adstrt_adcity_id;

    /**
     * The value for the adstrt_name field.
     *
     * @var        string
     */
    protected $adstrt_name;

    /**
     * The value for the adstrt_postcode field.
     *
     * @var        string
     */
    protected $adstrt_postcode;

    /**
     * The value for the adstrt_lat field.
     *
     * @var        double
     */
    protected $adstrt_lat;

    /**
     * The value for the adstrt_lon field.
     *
     * @var        double
     */
    protected $adstrt_lon;

    /**
     * The value for the adstrt_importance field.
     *
     * @var        double
     */
    protected $adstrt_importance;

    /**
     * The value for the adstrt_housenumbers_array field.
     *
     * @var        string
     */
    protected $adstrt_housenumbers_array;

    /**
     * @var        ChildType
     */
    protected $aType;

    /**
     * @var        ChildCity
     */
    protected $aCity;

    /**
     * @var        ObjectCollection|ChildHouseNumber[] Collection to store aggregation of ChildHouseNumber objects.
     */
    protected $collHouseNumbers;
    protected $collHouseNumbersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildHouseNumber[]
     */
    protected $houseNumbersScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\AddressBundle\Model\Base\Street object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Street</code> instance.  If
     * <code>obj</code> is an instance of <code>Street</code>, delegates to
     * <code>equals(Street)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Street The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [adstrt_id] column value.
     *
     * @return string
     */
    public function getId()
    {
        return $this->adstrt_id;
    }

    /**
     * Get the [adstrt_adtype_id] column value.
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->adstrt_adtype_id;
    }

    /**
     * Get the [adstrt_adcity_id] column value.
     *
     * @return string
     */
    public function getCityId()
    {
        return $this->adstrt_adcity_id;
    }

    /**
     * Get the [adstrt_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->adstrt_name;
    }

    /**
     * Get the [adstrt_postcode] column value.
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->adstrt_postcode;
    }

    /**
     * Get the [adstrt_lat] column value.
     *
     * @return double
     */
    public function getLat()
    {
        return $this->adstrt_lat;
    }

    /**
     * Get the [adstrt_lon] column value.
     *
     * @return double
     */
    public function getLon()
    {
        return $this->adstrt_lon;
    }

    /**
     * Get the [adstrt_importance] column value.
     *
     * @return double
     */
    public function getImportance()
    {
        return $this->adstrt_importance;
    }

    /**
     * Get the [adstrt_housenumbers_array] column value.
     *
     * @return string
     */
    public function getHouseNumbersArray()
    {
        return $this->adstrt_housenumbers_array;
    }

    /**
     * Set the value of [adstrt_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adstrt_id !== $v) {
            $this->adstrt_id = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [adstrt_adtype_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->adstrt_adtype_id !== $v) {
            $this->adstrt_adtype_id = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_ADTYPE_ID] = true;
        }

        if ($this->aType !== null && $this->aType->getId() !== $v) {
            $this->aType = null;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [adstrt_adcity_id] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setCityId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adstrt_adcity_id !== $v) {
            $this->adstrt_adcity_id = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_ADCITY_ID] = true;
        }

        if ($this->aCity !== null && $this->aCity->getId() !== $v) {
            $this->aCity = null;
        }

        return $this;
    } // setCityId()

    /**
     * Set the value of [adstrt_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adstrt_name !== $v) {
            $this->adstrt_name = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [adstrt_postcode] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setPostCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adstrt_postcode !== $v) {
            $this->adstrt_postcode = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_POSTCODE] = true;
        }

        return $this;
    } // setPostCode()

    /**
     * Set the value of [adstrt_lat] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adstrt_lat !== $v) {
            $this->adstrt_lat = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_LAT] = true;
        }

        return $this;
    } // setLat()

    /**
     * Set the value of [adstrt_lon] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setLon($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adstrt_lon !== $v) {
            $this->adstrt_lon = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_LON] = true;
        }

        return $this;
    } // setLon()

    /**
     * Set the value of [adstrt_importance] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setImportance($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->adstrt_importance !== $v) {
            $this->adstrt_importance = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_IMPORTANCE] = true;
        }

        return $this;
    } // setImportance()

    /**
     * Set the value of [adstrt_housenumbers_array] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function setHouseNumbersArray($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adstrt_housenumbers_array !== $v) {
            $this->adstrt_housenumbers_array = $v;
            $this->modifiedColumns[StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY] = true;
        }

        return $this;
    } // setHouseNumbersArray()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : StreetTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : StreetTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_adtype_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : StreetTableMap::translateFieldName('CityId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_adcity_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : StreetTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : StreetTableMap::translateFieldName('PostCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_postcode = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : StreetTableMap::translateFieldName('Lat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_lat = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : StreetTableMap::translateFieldName('Lon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_lon = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : StreetTableMap::translateFieldName('Importance', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_importance = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : StreetTableMap::translateFieldName('HouseNumbersArray', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adstrt_housenumbers_array = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = StreetTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\AddressBundle\\Model\\Street'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aType !== null && $this->adstrt_adtype_id !== $this->aType->getId()) {
            $this->aType = null;
        }
        if ($this->aCity !== null && $this->adstrt_adcity_id !== $this->aCity->getId()) {
            $this->aCity = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StreetTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildStreetQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aType = null;
            $this->aCity = null;
            $this->collHouseNumbers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Street::setDeleted()
     * @see Street::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildStreetQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StreetTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aType !== null) {
                if ($this->aType->isModified() || $this->aType->isNew()) {
                    $affectedRows += $this->aType->save($con);
                }
                $this->setType($this->aType);
            }

            if ($this->aCity !== null) {
                if ($this->aCity->isModified() || $this->aCity->isNew()) {
                    $affectedRows += $this->aCity->save($con);
                }
                $this->setCity($this->aCity);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->houseNumbersScheduledForDeletion !== null) {
                if (!$this->houseNumbersScheduledForDeletion->isEmpty()) {
                    \IiMedias\AddressBundle\Model\HouseNumberQuery::create()
                        ->filterByPrimaryKeys($this->houseNumbersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->houseNumbersScheduledForDeletion = null;
                }
            }

            if ($this->collHouseNumbers !== null) {
                foreach ($this->collHouseNumbers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_id';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ADTYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_adtype_id';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ADCITY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_adcity_id';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_name';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_POSTCODE)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_postcode';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_LAT)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_lat';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_LON)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_lon';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_IMPORTANCE)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_importance';
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY)) {
            $modifiedColumns[':p' . $index++]  = 'adstrt_housenumbers_array';
        }

        $sql = sprintf(
            'INSERT INTO address_street_adstrt (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'adstrt_id':
                        $stmt->bindValue($identifier, $this->adstrt_id, PDO::PARAM_STR);
                        break;
                    case 'adstrt_adtype_id':
                        $stmt->bindValue($identifier, $this->adstrt_adtype_id, PDO::PARAM_INT);
                        break;
                    case 'adstrt_adcity_id':
                        $stmt->bindValue($identifier, $this->adstrt_adcity_id, PDO::PARAM_STR);
                        break;
                    case 'adstrt_name':
                        $stmt->bindValue($identifier, $this->adstrt_name, PDO::PARAM_STR);
                        break;
                    case 'adstrt_postcode':
                        $stmt->bindValue($identifier, $this->adstrt_postcode, PDO::PARAM_STR);
                        break;
                    case 'adstrt_lat':
                        $stmt->bindValue($identifier, $this->adstrt_lat, PDO::PARAM_STR);
                        break;
                    case 'adstrt_lon':
                        $stmt->bindValue($identifier, $this->adstrt_lon, PDO::PARAM_STR);
                        break;
                    case 'adstrt_importance':
                        $stmt->bindValue($identifier, $this->adstrt_importance, PDO::PARAM_STR);
                        break;
                    case 'adstrt_housenumbers_array':
                        $stmt->bindValue($identifier, $this->adstrt_housenumbers_array, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StreetTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTypeId();
                break;
            case 2:
                return $this->getCityId();
                break;
            case 3:
                return $this->getName();
                break;
            case 4:
                return $this->getPostCode();
                break;
            case 5:
                return $this->getLat();
                break;
            case 6:
                return $this->getLon();
                break;
            case 7:
                return $this->getImportance();
                break;
            case 8:
                return $this->getHouseNumbersArray();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Street'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Street'][$this->hashCode()] = true;
        $keys = StreetTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTypeId(),
            $keys[2] => $this->getCityId(),
            $keys[3] => $this->getName(),
            $keys[4] => $this->getPostCode(),
            $keys[5] => $this->getLat(),
            $keys[6] => $this->getLon(),
            $keys[7] => $this->getImportance(),
            $keys[8] => $this->getHouseNumbersArray(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'type';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_type_adtype';
                        break;
                    default:
                        $key = 'Type';
                }

                $result[$key] = $this->aType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCity) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'city';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_city_adcity';
                        break;
                    default:
                        $key = 'City';
                }

                $result[$key] = $this->aCity->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collHouseNumbers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'houseNumbers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address_house_number_adhsnbs';
                        break;
                    default:
                        $key = 'HouseNumbers';
                }

                $result[$key] = $this->collHouseNumbers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\AddressBundle\Model\Street
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StreetTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\AddressBundle\Model\Street
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTypeId($value);
                break;
            case 2:
                $this->setCityId($value);
                break;
            case 3:
                $this->setName($value);
                break;
            case 4:
                $this->setPostCode($value);
                break;
            case 5:
                $this->setLat($value);
                break;
            case 6:
                $this->setLon($value);
                break;
            case 7:
                $this->setImportance($value);
                break;
            case 8:
                $this->setHouseNumbersArray($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = StreetTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTypeId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCityId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPostCode($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLat($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setLon($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImportance($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHouseNumbersArray($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StreetTableMap::DATABASE_NAME);

        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ID)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_ID, $this->adstrt_id);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ADTYPE_ID)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_ADTYPE_ID, $this->adstrt_adtype_id);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_ADCITY_ID)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_ADCITY_ID, $this->adstrt_adcity_id);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_NAME)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_NAME, $this->adstrt_name);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_POSTCODE)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_POSTCODE, $this->adstrt_postcode);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_LAT)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_LAT, $this->adstrt_lat);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_LON)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_LON, $this->adstrt_lon);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_IMPORTANCE)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_IMPORTANCE, $this->adstrt_importance);
        }
        if ($this->isColumnModified(StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY)) {
            $criteria->add(StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY, $this->adstrt_housenumbers_array);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildStreetQuery::create();
        $criteria->add(StreetTableMap::COL_ADSTRT_ID, $this->adstrt_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (adstrt_id column).
     *
     * @param       string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\AddressBundle\Model\Street (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setId($this->getId());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setCityId($this->getCityId());
        $copyObj->setName($this->getName());
        $copyObj->setPostCode($this->getPostCode());
        $copyObj->setLat($this->getLat());
        $copyObj->setLon($this->getLon());
        $copyObj->setImportance($this->getImportance());
        $copyObj->setHouseNumbersArray($this->getHouseNumbersArray());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getHouseNumbers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addHouseNumber($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\AddressBundle\Model\Street Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildType object.
     *
     * @param  ChildType $v
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     * @throws PropelException
     */
    public function setType(ChildType $v = null)
    {
        if ($v === null) {
            $this->setTypeId(NULL);
        } else {
            $this->setTypeId($v->getId());
        }

        $this->aType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildType object, it will not be re-added.
        if ($v !== null) {
            $v->addStreet($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildType The associated ChildType object.
     * @throws PropelException
     */
    public function getType(ConnectionInterface $con = null)
    {
        if ($this->aType === null && ($this->adstrt_adtype_id !== null)) {
            $this->aType = ChildTypeQuery::create()->findPk($this->adstrt_adtype_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aType->addStreets($this);
             */
        }

        return $this->aType;
    }

    /**
     * Declares an association between this object and a ChildCity object.
     *
     * @param  ChildCity $v
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCity(ChildCity $v = null)
    {
        if ($v === null) {
            $this->setCityId(NULL);
        } else {
            $this->setCityId($v->getId());
        }

        $this->aCity = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCity object, it will not be re-added.
        if ($v !== null) {
            $v->addStreet($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCity object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCity The associated ChildCity object.
     * @throws PropelException
     */
    public function getCity(ConnectionInterface $con = null)
    {
        if ($this->aCity === null && (($this->adstrt_adcity_id !== "" && $this->adstrt_adcity_id !== null))) {
            $this->aCity = ChildCityQuery::create()->findPk($this->adstrt_adcity_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCity->addStreets($this);
             */
        }

        return $this->aCity;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('HouseNumber' == $relationName) {
            return $this->initHouseNumbers();
        }
    }

    /**
     * Clears out the collHouseNumbers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addHouseNumbers()
     */
    public function clearHouseNumbers()
    {
        $this->collHouseNumbers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collHouseNumbers collection loaded partially.
     */
    public function resetPartialHouseNumbers($v = true)
    {
        $this->collHouseNumbersPartial = $v;
    }

    /**
     * Initializes the collHouseNumbers collection.
     *
     * By default this just sets the collHouseNumbers collection to an empty array (like clearcollHouseNumbers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initHouseNumbers($overrideExisting = true)
    {
        if (null !== $this->collHouseNumbers && !$overrideExisting) {
            return;
        }

        $collectionClassName = HouseNumberTableMap::getTableMap()->getCollectionClassName();

        $this->collHouseNumbers = new $collectionClassName;
        $this->collHouseNumbers->setModel('\IiMedias\AddressBundle\Model\HouseNumber');
    }

    /**
     * Gets an array of ChildHouseNumber objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildStreet is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildHouseNumber[] List of ChildHouseNumber objects
     * @throws PropelException
     */
    public function getHouseNumbers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collHouseNumbersPartial && !$this->isNew();
        if (null === $this->collHouseNumbers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collHouseNumbers) {
                // return empty collection
                $this->initHouseNumbers();
            } else {
                $collHouseNumbers = ChildHouseNumberQuery::create(null, $criteria)
                    ->filterByStreet($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collHouseNumbersPartial && count($collHouseNumbers)) {
                        $this->initHouseNumbers(false);

                        foreach ($collHouseNumbers as $obj) {
                            if (false == $this->collHouseNumbers->contains($obj)) {
                                $this->collHouseNumbers->append($obj);
                            }
                        }

                        $this->collHouseNumbersPartial = true;
                    }

                    return $collHouseNumbers;
                }

                if ($partial && $this->collHouseNumbers) {
                    foreach ($this->collHouseNumbers as $obj) {
                        if ($obj->isNew()) {
                            $collHouseNumbers[] = $obj;
                        }
                    }
                }

                $this->collHouseNumbers = $collHouseNumbers;
                $this->collHouseNumbersPartial = false;
            }
        }

        return $this->collHouseNumbers;
    }

    /**
     * Sets a collection of ChildHouseNumber objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $houseNumbers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildStreet The current object (for fluent API support)
     */
    public function setHouseNumbers(Collection $houseNumbers, ConnectionInterface $con = null)
    {
        /** @var ChildHouseNumber[] $houseNumbersToDelete */
        $houseNumbersToDelete = $this->getHouseNumbers(new Criteria(), $con)->diff($houseNumbers);


        $this->houseNumbersScheduledForDeletion = $houseNumbersToDelete;

        foreach ($houseNumbersToDelete as $houseNumberRemoved) {
            $houseNumberRemoved->setStreet(null);
        }

        $this->collHouseNumbers = null;
        foreach ($houseNumbers as $houseNumber) {
            $this->addHouseNumber($houseNumber);
        }

        $this->collHouseNumbers = $houseNumbers;
        $this->collHouseNumbersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related HouseNumber objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related HouseNumber objects.
     * @throws PropelException
     */
    public function countHouseNumbers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collHouseNumbersPartial && !$this->isNew();
        if (null === $this->collHouseNumbers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collHouseNumbers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getHouseNumbers());
            }

            $query = ChildHouseNumberQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStreet($this)
                ->count($con);
        }

        return count($this->collHouseNumbers);
    }

    /**
     * Method called to associate a ChildHouseNumber object to this object
     * through the ChildHouseNumber foreign key attribute.
     *
     * @param  ChildHouseNumber $l ChildHouseNumber
     * @return $this|\IiMedias\AddressBundle\Model\Street The current object (for fluent API support)
     */
    public function addHouseNumber(ChildHouseNumber $l)
    {
        if ($this->collHouseNumbers === null) {
            $this->initHouseNumbers();
            $this->collHouseNumbersPartial = true;
        }

        if (!$this->collHouseNumbers->contains($l)) {
            $this->doAddHouseNumber($l);

            if ($this->houseNumbersScheduledForDeletion and $this->houseNumbersScheduledForDeletion->contains($l)) {
                $this->houseNumbersScheduledForDeletion->remove($this->houseNumbersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildHouseNumber $houseNumber The ChildHouseNumber object to add.
     */
    protected function doAddHouseNumber(ChildHouseNumber $houseNumber)
    {
        $this->collHouseNumbers[]= $houseNumber;
        $houseNumber->setStreet($this);
    }

    /**
     * @param  ChildHouseNumber $houseNumber The ChildHouseNumber object to remove.
     * @return $this|ChildStreet The current object (for fluent API support)
     */
    public function removeHouseNumber(ChildHouseNumber $houseNumber)
    {
        if ($this->getHouseNumbers()->contains($houseNumber)) {
            $pos = $this->collHouseNumbers->search($houseNumber);
            $this->collHouseNumbers->remove($pos);
            if (null === $this->houseNumbersScheduledForDeletion) {
                $this->houseNumbersScheduledForDeletion = clone $this->collHouseNumbers;
                $this->houseNumbersScheduledForDeletion->clear();
            }
            $this->houseNumbersScheduledForDeletion[]= clone $houseNumber;
            $houseNumber->setStreet(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aType) {
            $this->aType->removeStreet($this);
        }
        if (null !== $this->aCity) {
            $this->aCity->removeStreet($this);
        }
        $this->adstrt_id = null;
        $this->adstrt_adtype_id = null;
        $this->adstrt_adcity_id = null;
        $this->adstrt_name = null;
        $this->adstrt_postcode = null;
        $this->adstrt_lat = null;
        $this->adstrt_lon = null;
        $this->adstrt_importance = null;
        $this->adstrt_housenumbers_array = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collHouseNumbers) {
                foreach ($this->collHouseNumbers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collHouseNumbers = null;
        $this->aType = null;
        $this->aCity = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StreetTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
