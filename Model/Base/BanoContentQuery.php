<?php

namespace IiMedias\AddressBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AddressBundle\Model\BanoContent as ChildBanoContent;
use IiMedias\AddressBundle\Model\BanoContentQuery as ChildBanoContentQuery;
use IiMedias\AddressBundle\Model\Map\BanoContentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'address_bano_content_adbctn' table.
 *
 *
 *
 * @method     ChildBanoContentQuery orderById($order = Criteria::ASC) Order by the adbctn_id column
 * @method     ChildBanoContentQuery orderByType($order = Criteria::ASC) Order by the adbctn_type column
 * @method     ChildBanoContentQuery orderByContent($order = Criteria::ASC) Order by the adbctn_content column
 * @method     ChildBanoContentQuery orderByProcessed($order = Criteria::ASC) Order by the adbctn_processed column
 * @method     ChildBanoContentQuery orderByError($order = Criteria::ASC) Order by the adbctn_error column
 *
 * @method     ChildBanoContentQuery groupById() Group by the adbctn_id column
 * @method     ChildBanoContentQuery groupByType() Group by the adbctn_type column
 * @method     ChildBanoContentQuery groupByContent() Group by the adbctn_content column
 * @method     ChildBanoContentQuery groupByProcessed() Group by the adbctn_processed column
 * @method     ChildBanoContentQuery groupByError() Group by the adbctn_error column
 *
 * @method     ChildBanoContentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBanoContentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBanoContentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBanoContentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBanoContentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBanoContentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBanoContent findOne(ConnectionInterface $con = null) Return the first ChildBanoContent matching the query
 * @method     ChildBanoContent findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBanoContent matching the query, or a new ChildBanoContent object populated from the query conditions when no match is found
 *
 * @method     ChildBanoContent findOneById(int $adbctn_id) Return the first ChildBanoContent filtered by the adbctn_id column
 * @method     ChildBanoContent findOneByType(string $adbctn_type) Return the first ChildBanoContent filtered by the adbctn_type column
 * @method     ChildBanoContent findOneByContent(string $adbctn_content) Return the first ChildBanoContent filtered by the adbctn_content column
 * @method     ChildBanoContent findOneByProcessed(boolean $adbctn_processed) Return the first ChildBanoContent filtered by the adbctn_processed column
 * @method     ChildBanoContent findOneByError(boolean $adbctn_error) Return the first ChildBanoContent filtered by the adbctn_error column *

 * @method     ChildBanoContent requirePk($key, ConnectionInterface $con = null) Return the ChildBanoContent by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanoContent requireOne(ConnectionInterface $con = null) Return the first ChildBanoContent matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBanoContent requireOneById(int $adbctn_id) Return the first ChildBanoContent filtered by the adbctn_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanoContent requireOneByType(string $adbctn_type) Return the first ChildBanoContent filtered by the adbctn_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanoContent requireOneByContent(string $adbctn_content) Return the first ChildBanoContent filtered by the adbctn_content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanoContent requireOneByProcessed(boolean $adbctn_processed) Return the first ChildBanoContent filtered by the adbctn_processed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanoContent requireOneByError(boolean $adbctn_error) Return the first ChildBanoContent filtered by the adbctn_error column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBanoContent[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBanoContent objects based on current ModelCriteria
 * @method     ChildBanoContent[]|ObjectCollection findById(int $adbctn_id) Return ChildBanoContent objects filtered by the adbctn_id column
 * @method     ChildBanoContent[]|ObjectCollection findByType(string $adbctn_type) Return ChildBanoContent objects filtered by the adbctn_type column
 * @method     ChildBanoContent[]|ObjectCollection findByContent(string $adbctn_content) Return ChildBanoContent objects filtered by the adbctn_content column
 * @method     ChildBanoContent[]|ObjectCollection findByProcessed(boolean $adbctn_processed) Return ChildBanoContent objects filtered by the adbctn_processed column
 * @method     ChildBanoContent[]|ObjectCollection findByError(boolean $adbctn_error) Return ChildBanoContent objects filtered by the adbctn_error column
 * @method     ChildBanoContent[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BanoContentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\AddressBundle\Model\Base\BanoContentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\AddressBundle\\Model\\BanoContent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBanoContentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBanoContentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBanoContentQuery) {
            return $criteria;
        }
        $query = new ChildBanoContentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBanoContent|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BanoContentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BanoContentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBanoContent A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT adbctn_id, adbctn_type, adbctn_content, adbctn_processed, adbctn_error FROM address_bano_content_adbctn WHERE adbctn_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBanoContent $obj */
            $obj = new ChildBanoContent();
            $obj->hydrate($row);
            BanoContentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBanoContent|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the adbctn_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE adbctn_id = 1234
     * $query->filterById(array(12, 34)); // WHERE adbctn_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE adbctn_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $id, $comparison);
    }

    /**
     * Filter the query on the adbctn_type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE adbctn_type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE adbctn_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the adbctn_content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE adbctn_content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE adbctn_content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the adbctn_processed column
     *
     * Example usage:
     * <code>
     * $query->filterByProcessed(true); // WHERE adbctn_processed = true
     * $query->filterByProcessed('yes'); // WHERE adbctn_processed = true
     * </code>
     *
     * @param     boolean|string $processed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByProcessed($processed = null, $comparison = null)
    {
        if (is_string($processed)) {
            $processed = in_array(strtolower($processed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_PROCESSED, $processed, $comparison);
    }

    /**
     * Filter the query on the adbctn_error column
     *
     * Example usage:
     * <code>
     * $query->filterByError(true); // WHERE adbctn_error = true
     * $query->filterByError('yes'); // WHERE adbctn_error = true
     * </code>
     *
     * @param     boolean|string $error The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function filterByError($error = null, $comparison = null)
    {
        if (is_string($error)) {
            $error = in_array(strtolower($error), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ERROR, $error, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBanoContent $banoContent Object to remove from the list of results
     *
     * @return $this|ChildBanoContentQuery The current query, for fluid interface
     */
    public function prune($banoContent = null)
    {
        if ($banoContent) {
            $this->addUsingAlias(BanoContentTableMap::COL_ADBCTN_ID, $banoContent->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the address_bano_content_adbctn table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BanoContentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BanoContentTableMap::clearInstancePool();
            BanoContentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BanoContentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BanoContentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BanoContentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BanoContentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BanoContentQuery
