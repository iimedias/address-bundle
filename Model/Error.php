<?php

namespace IiMedias\AddressBundle\Model;

use IiMedias\AddressBundle\Model\Base\Error as BaseError;

/**
 * Skeleton subclass for representing a row from the 'address_error_aderro' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Error extends BaseError
{

}
