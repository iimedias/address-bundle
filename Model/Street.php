<?php

namespace IiMedias\AddressBundle\Model;

use IiMedias\AddressBundle\Model\Base\Street as BaseStreet;

/**
 * Skeleton subclass for representing a row from the 'address_street_adstrt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Street extends BaseStreet
{

}
