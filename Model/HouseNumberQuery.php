<?php

namespace IiMedias\AddressBundle\Model;

use IiMedias\AddressBundle\Model\Base\HouseNumberQuery as BaseHouseNumberQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'address_house_number_adhsnb' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class HouseNumberQuery extends BaseHouseNumberQuery
{

}
