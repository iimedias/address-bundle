<?php

namespace IiMedias\AddressBundle\Model\Map;

use IiMedias\AddressBundle\Model\Bano;
use IiMedias\AddressBundle\Model\BanoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'address_bano_adbano' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BanoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AddressBundle.Model.Map.BanoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'address_bano_adbano';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AddressBundle\\Model\\Bano';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AddressBundle.Model.Bano';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the adbano_id field
     */
    const COL_ADBANO_ID = 'address_bano_adbano.adbano_id';

    /**
     * the column name for the adbano_departement_code field
     */
    const COL_ADBANO_DEPARTEMENT_CODE = 'address_bano_adbano.adbano_departement_code';

    /**
     * the column name for the adbano_scan field
     */
    const COL_ADBANO_SCAN = 'address_bano_adbano.adbano_scan';

    /**
     * the column name for the adbano_in_use field
     */
    const COL_ADBANO_IN_USE = 'address_bano_adbano.adbano_in_use';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'DepartementCode', 'Scan', 'InUse', ),
        self::TYPE_CAMELNAME     => array('id', 'departementCode', 'scan', 'inUse', ),
        self::TYPE_COLNAME       => array(BanoTableMap::COL_ADBANO_ID, BanoTableMap::COL_ADBANO_DEPARTEMENT_CODE, BanoTableMap::COL_ADBANO_SCAN, BanoTableMap::COL_ADBANO_IN_USE, ),
        self::TYPE_FIELDNAME     => array('adbano_id', 'adbano_departement_code', 'adbano_scan', 'adbano_in_use', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'DepartementCode' => 1, 'Scan' => 2, 'InUse' => 3, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'departementCode' => 1, 'scan' => 2, 'inUse' => 3, ),
        self::TYPE_COLNAME       => array(BanoTableMap::COL_ADBANO_ID => 0, BanoTableMap::COL_ADBANO_DEPARTEMENT_CODE => 1, BanoTableMap::COL_ADBANO_SCAN => 2, BanoTableMap::COL_ADBANO_IN_USE => 3, ),
        self::TYPE_FIELDNAME     => array('adbano_id' => 0, 'adbano_departement_code' => 1, 'adbano_scan' => 2, 'adbano_in_use' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address_bano_adbano');
        $this->setPhpName('Bano');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AddressBundle\\Model\\Bano');
        $this->setPackage('src.IiMedias.AddressBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('adbano_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('adbano_departement_code', 'DepartementCode', 'VARCHAR', true, 255, null);
        $this->addColumn('adbano_scan', 'Scan', 'BOOLEAN', true, 1, true);
        $this->addColumn('adbano_in_use', 'InUse', 'BOOLEAN', true, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BanoTableMap::CLASS_DEFAULT : BanoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Bano object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BanoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BanoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BanoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BanoTableMap::OM_CLASS;
            /** @var Bano $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BanoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BanoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BanoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Bano $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BanoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BanoTableMap::COL_ADBANO_ID);
            $criteria->addSelectColumn(BanoTableMap::COL_ADBANO_DEPARTEMENT_CODE);
            $criteria->addSelectColumn(BanoTableMap::COL_ADBANO_SCAN);
            $criteria->addSelectColumn(BanoTableMap::COL_ADBANO_IN_USE);
        } else {
            $criteria->addSelectColumn($alias . '.adbano_id');
            $criteria->addSelectColumn($alias . '.adbano_departement_code');
            $criteria->addSelectColumn($alias . '.adbano_scan');
            $criteria->addSelectColumn($alias . '.adbano_in_use');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BanoTableMap::DATABASE_NAME)->getTable(BanoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BanoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BanoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BanoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Bano or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Bano object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BanoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AddressBundle\Model\Bano) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BanoTableMap::DATABASE_NAME);
            $criteria->add(BanoTableMap::COL_ADBANO_ID, (array) $values, Criteria::IN);
        }

        $query = BanoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BanoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BanoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the address_bano_adbano table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BanoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Bano or Criteria object.
     *
     * @param mixed               $criteria Criteria or Bano object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BanoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Bano object
        }

        if ($criteria->containsKey(BanoTableMap::COL_ADBANO_ID) && $criteria->keyContainsValue(BanoTableMap::COL_ADBANO_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BanoTableMap::COL_ADBANO_ID.')');
        }


        // Set the correct dbName
        $query = BanoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BanoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BanoTableMap::buildTableMap();
