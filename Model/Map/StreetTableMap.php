<?php

namespace IiMedias\AddressBundle\Model\Map;

use IiMedias\AddressBundle\Model\Street;
use IiMedias\AddressBundle\Model\StreetQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'address_street_adstrt' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StreetTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AddressBundle.Model.Map.StreetTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'address_street_adstrt';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AddressBundle\\Model\\Street';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AddressBundle.Model.Street';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the adstrt_id field
     */
    const COL_ADSTRT_ID = 'address_street_adstrt.adstrt_id';

    /**
     * the column name for the adstrt_adtype_id field
     */
    const COL_ADSTRT_ADTYPE_ID = 'address_street_adstrt.adstrt_adtype_id';

    /**
     * the column name for the adstrt_adcity_id field
     */
    const COL_ADSTRT_ADCITY_ID = 'address_street_adstrt.adstrt_adcity_id';

    /**
     * the column name for the adstrt_name field
     */
    const COL_ADSTRT_NAME = 'address_street_adstrt.adstrt_name';

    /**
     * the column name for the adstrt_postcode field
     */
    const COL_ADSTRT_POSTCODE = 'address_street_adstrt.adstrt_postcode';

    /**
     * the column name for the adstrt_lat field
     */
    const COL_ADSTRT_LAT = 'address_street_adstrt.adstrt_lat';

    /**
     * the column name for the adstrt_lon field
     */
    const COL_ADSTRT_LON = 'address_street_adstrt.adstrt_lon';

    /**
     * the column name for the adstrt_importance field
     */
    const COL_ADSTRT_IMPORTANCE = 'address_street_adstrt.adstrt_importance';

    /**
     * the column name for the adstrt_housenumbers_array field
     */
    const COL_ADSTRT_HOUSENUMBERS_ARRAY = 'address_street_adstrt.adstrt_housenumbers_array';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TypeId', 'CityId', 'Name', 'PostCode', 'Lat', 'Lon', 'Importance', 'HouseNumbersArray', ),
        self::TYPE_CAMELNAME     => array('id', 'typeId', 'cityId', 'name', 'postCode', 'lat', 'lon', 'importance', 'houseNumbersArray', ),
        self::TYPE_COLNAME       => array(StreetTableMap::COL_ADSTRT_ID, StreetTableMap::COL_ADSTRT_ADTYPE_ID, StreetTableMap::COL_ADSTRT_ADCITY_ID, StreetTableMap::COL_ADSTRT_NAME, StreetTableMap::COL_ADSTRT_POSTCODE, StreetTableMap::COL_ADSTRT_LAT, StreetTableMap::COL_ADSTRT_LON, StreetTableMap::COL_ADSTRT_IMPORTANCE, StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY, ),
        self::TYPE_FIELDNAME     => array('adstrt_id', 'adstrt_adtype_id', 'adstrt_adcity_id', 'adstrt_name', 'adstrt_postcode', 'adstrt_lat', 'adstrt_lon', 'adstrt_importance', 'adstrt_housenumbers_array', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TypeId' => 1, 'CityId' => 2, 'Name' => 3, 'PostCode' => 4, 'Lat' => 5, 'Lon' => 6, 'Importance' => 7, 'HouseNumbersArray' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'typeId' => 1, 'cityId' => 2, 'name' => 3, 'postCode' => 4, 'lat' => 5, 'lon' => 6, 'importance' => 7, 'houseNumbersArray' => 8, ),
        self::TYPE_COLNAME       => array(StreetTableMap::COL_ADSTRT_ID => 0, StreetTableMap::COL_ADSTRT_ADTYPE_ID => 1, StreetTableMap::COL_ADSTRT_ADCITY_ID => 2, StreetTableMap::COL_ADSTRT_NAME => 3, StreetTableMap::COL_ADSTRT_POSTCODE => 4, StreetTableMap::COL_ADSTRT_LAT => 5, StreetTableMap::COL_ADSTRT_LON => 6, StreetTableMap::COL_ADSTRT_IMPORTANCE => 7, StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY => 8, ),
        self::TYPE_FIELDNAME     => array('adstrt_id' => 0, 'adstrt_adtype_id' => 1, 'adstrt_adcity_id' => 2, 'adstrt_name' => 3, 'adstrt_postcode' => 4, 'adstrt_lat' => 5, 'adstrt_lon' => 6, 'adstrt_importance' => 7, 'adstrt_housenumbers_array' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address_street_adstrt');
        $this->setPhpName('Street');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AddressBundle\\Model\\Street');
        $this->setPackage('src.IiMedias.AddressBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('adstrt_id', 'Id', 'VARCHAR', true, 255, null);
        $this->addForeignKey('adstrt_adtype_id', 'TypeId', 'INTEGER', 'address_type_adtype', 'adtype_id', true, null, null);
        $this->addForeignKey('adstrt_adcity_id', 'CityId', 'VARCHAR', 'address_city_adcity', 'adcity_id', true, 255, null);
        $this->addColumn('adstrt_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('adstrt_postcode', 'PostCode', 'VARCHAR', true, 255, null);
        $this->addColumn('adstrt_lat', 'Lat', 'FLOAT', true, null, null);
        $this->addColumn('adstrt_lon', 'Lon', 'FLOAT', true, null, null);
        $this->addColumn('adstrt_importance', 'Importance', 'FLOAT', true, null, null);
        $this->addColumn('adstrt_housenumbers_array', 'HouseNumbersArray', 'LONGVARCHAR', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Type', '\\IiMedias\\AddressBundle\\Model\\Type', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':adstrt_adtype_id',
    1 => ':adtype_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('City', '\\IiMedias\\AddressBundle\\Model\\City', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':adstrt_adcity_id',
    1 => ':adcity_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('HouseNumber', '\\IiMedias\\AddressBundle\\Model\\HouseNumber', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':adhsnb_adstrt_id',
    1 => ':adstrt_id',
  ),
), 'CASCADE', 'CASCADE', 'HouseNumbers', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to address_street_adstrt     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        HouseNumberTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StreetTableMap::CLASS_DEFAULT : StreetTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Street object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StreetTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StreetTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StreetTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StreetTableMap::OM_CLASS;
            /** @var Street $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StreetTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StreetTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StreetTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Street $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StreetTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_ID);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_ADTYPE_ID);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_ADCITY_ID);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_NAME);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_POSTCODE);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_LAT);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_LON);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_IMPORTANCE);
            $criteria->addSelectColumn(StreetTableMap::COL_ADSTRT_HOUSENUMBERS_ARRAY);
        } else {
            $criteria->addSelectColumn($alias . '.adstrt_id');
            $criteria->addSelectColumn($alias . '.adstrt_adtype_id');
            $criteria->addSelectColumn($alias . '.adstrt_adcity_id');
            $criteria->addSelectColumn($alias . '.adstrt_name');
            $criteria->addSelectColumn($alias . '.adstrt_postcode');
            $criteria->addSelectColumn($alias . '.adstrt_lat');
            $criteria->addSelectColumn($alias . '.adstrt_lon');
            $criteria->addSelectColumn($alias . '.adstrt_importance');
            $criteria->addSelectColumn($alias . '.adstrt_housenumbers_array');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StreetTableMap::DATABASE_NAME)->getTable(StreetTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StreetTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StreetTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StreetTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Street or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Street object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AddressBundle\Model\Street) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StreetTableMap::DATABASE_NAME);
            $criteria->add(StreetTableMap::COL_ADSTRT_ID, (array) $values, Criteria::IN);
        }

        $query = StreetQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StreetTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StreetTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the address_street_adstrt table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StreetQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Street or Criteria object.
     *
     * @param mixed               $criteria Criteria or Street object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Street object
        }


        // Set the correct dbName
        $query = StreetQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StreetTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StreetTableMap::buildTableMap();
