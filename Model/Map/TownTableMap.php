<?php

namespace IiMedias\AddressBundle\Model\Map;

use IiMedias\AddressBundle\Model\Town;
use IiMedias\AddressBundle\Model\TownQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'address_town_adtown' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TownTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AddressBundle.Model.Map.TownTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'address_town_adtown';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AddressBundle\\Model\\Town';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AddressBundle.Model.Town';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the adtown_id field
     */
    const COL_ADTOWN_ID = 'address_town_adtown.adtown_id';

    /**
     * the column name for the adtown_type field
     */
    const COL_ADTOWN_TYPE = 'address_town_adtown.adtown_type';

    /**
     * the column name for the adtown_name field
     */
    const COL_ADTOWN_NAME = 'address_town_adtown.adtown_name';

    /**
     * the column name for the adtown_postcode field
     */
    const COL_ADTOWN_POSTCODE = 'address_town_adtown.adtown_postcode';

    /**
     * the column name for the adtown_lat field
     */
    const COL_ADTOWN_LAT = 'address_town_adtown.adtown_lat';

    /**
     * the column name for the adtown_lon field
     */
    const COL_ADTOWN_LON = 'address_town_adtown.adtown_lon';

    /**
     * the column name for the adtown_city field
     */
    const COL_ADTOWN_CITY = 'address_town_adtown.adtown_city';

    /**
     * the column name for the adtown_departement field
     */
    const COL_ADTOWN_DEPARTEMENT = 'address_town_adtown.adtown_departement';

    /**
     * the column name for the adtown_region field
     */
    const COL_ADTOWN_REGION = 'address_town_adtown.adtown_region';

    /**
     * the column name for the adtown_pop field
     */
    const COL_ADTOWN_POP = 'address_town_adtown.adtown_pop';

    /**
     * the column name for the adtown_adm_weight field
     */
    const COL_ADTOWN_ADM_WEIGHT = 'address_town_adtown.adtown_adm_weight';

    /**
     * the column name for the adtown_importance field
     */
    const COL_ADTOWN_IMPORTANCE = 'address_town_adtown.adtown_importance';

    /**
     * the column name for the adtown_created_at field
     */
    const COL_ADTOWN_CREATED_AT = 'address_town_adtown.adtown_created_at';

    /**
     * the column name for the adtown_updated_at field
     */
    const COL_ADTOWN_UPDATED_AT = 'address_town_adtown.adtown_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Type', 'Name', 'PostCode', 'Lat', 'Lon', 'City', 'Departement', 'Region', 'Pop', 'AdmWeight', 'Importance', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'type', 'name', 'postCode', 'lat', 'lon', 'city', 'departement', 'region', 'pop', 'admWeight', 'importance', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(TownTableMap::COL_ADTOWN_ID, TownTableMap::COL_ADTOWN_TYPE, TownTableMap::COL_ADTOWN_NAME, TownTableMap::COL_ADTOWN_POSTCODE, TownTableMap::COL_ADTOWN_LAT, TownTableMap::COL_ADTOWN_LON, TownTableMap::COL_ADTOWN_CITY, TownTableMap::COL_ADTOWN_DEPARTEMENT, TownTableMap::COL_ADTOWN_REGION, TownTableMap::COL_ADTOWN_POP, TownTableMap::COL_ADTOWN_ADM_WEIGHT, TownTableMap::COL_ADTOWN_IMPORTANCE, TownTableMap::COL_ADTOWN_CREATED_AT, TownTableMap::COL_ADTOWN_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('adtown_id', 'adtown_type', 'adtown_name', 'adtown_postcode', 'adtown_lat', 'adtown_lon', 'adtown_city', 'adtown_departement', 'adtown_region', 'adtown_pop', 'adtown_adm_weight', 'adtown_importance', 'adtown_created_at', 'adtown_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Type' => 1, 'Name' => 2, 'PostCode' => 3, 'Lat' => 4, 'Lon' => 5, 'City' => 6, 'Departement' => 7, 'Region' => 8, 'Pop' => 9, 'AdmWeight' => 10, 'Importance' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'type' => 1, 'name' => 2, 'postCode' => 3, 'lat' => 4, 'lon' => 5, 'city' => 6, 'departement' => 7, 'region' => 8, 'pop' => 9, 'admWeight' => 10, 'importance' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(TownTableMap::COL_ADTOWN_ID => 0, TownTableMap::COL_ADTOWN_TYPE => 1, TownTableMap::COL_ADTOWN_NAME => 2, TownTableMap::COL_ADTOWN_POSTCODE => 3, TownTableMap::COL_ADTOWN_LAT => 4, TownTableMap::COL_ADTOWN_LON => 5, TownTableMap::COL_ADTOWN_CITY => 6, TownTableMap::COL_ADTOWN_DEPARTEMENT => 7, TownTableMap::COL_ADTOWN_REGION => 8, TownTableMap::COL_ADTOWN_POP => 9, TownTableMap::COL_ADTOWN_ADM_WEIGHT => 10, TownTableMap::COL_ADTOWN_IMPORTANCE => 11, TownTableMap::COL_ADTOWN_CREATED_AT => 12, TownTableMap::COL_ADTOWN_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('adtown_id' => 0, 'adtown_type' => 1, 'adtown_name' => 2, 'adtown_postcode' => 3, 'adtown_lat' => 4, 'adtown_lon' => 5, 'adtown_city' => 6, 'adtown_departement' => 7, 'adtown_region' => 8, 'adtown_pop' => 9, 'adtown_adm_weight' => 10, 'adtown_importance' => 11, 'adtown_created_at' => 12, 'adtown_updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address_town_adtown');
        $this->setPhpName('Town');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AddressBundle\\Model\\Town');
        $this->setPackage('src.IiMedias.AddressBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('adtown_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('adtown_type', 'Type', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_postcode', 'PostCode', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_lat', 'Lat', 'FLOAT', true, null, null);
        $this->addColumn('adtown_lon', 'Lon', 'FLOAT', true, null, null);
        $this->addColumn('adtown_city', 'City', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_departement', 'Departement', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_region', 'Region', 'VARCHAR', true, 255, null);
        $this->addColumn('adtown_pop', 'Pop', 'FLOAT', true, null, null);
        $this->addColumn('adtown_adm_weight', 'AdmWeight', 'FLOAT', true, null, null);
        $this->addColumn('adtown_importance', 'Importance', 'FLOAT', true, null, null);
        $this->addColumn('adtown_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('adtown_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Street', '\\IiMedias\\AddressBundle\\Model\\Street', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':adstrt_adtown_id',
    1 => ':adtown_id',
  ),
), 'CASCADE', 'CASCADE', 'Streets', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'adtown_created_at', 'update_column' => 'adtown_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to address_town_adtown     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        StreetTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TownTableMap::CLASS_DEFAULT : TownTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Town object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TownTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TownTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TownTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TownTableMap::OM_CLASS;
            /** @var Town $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TownTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TownTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TownTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Town $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TownTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_ID);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_TYPE);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_NAME);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_POSTCODE);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_LAT);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_LON);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_CITY);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_DEPARTEMENT);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_REGION);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_POP);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_ADM_WEIGHT);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_IMPORTANCE);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_CREATED_AT);
            $criteria->addSelectColumn(TownTableMap::COL_ADTOWN_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.adtown_id');
            $criteria->addSelectColumn($alias . '.adtown_type');
            $criteria->addSelectColumn($alias . '.adtown_name');
            $criteria->addSelectColumn($alias . '.adtown_postcode');
            $criteria->addSelectColumn($alias . '.adtown_lat');
            $criteria->addSelectColumn($alias . '.adtown_lon');
            $criteria->addSelectColumn($alias . '.adtown_city');
            $criteria->addSelectColumn($alias . '.adtown_departement');
            $criteria->addSelectColumn($alias . '.adtown_region');
            $criteria->addSelectColumn($alias . '.adtown_pop');
            $criteria->addSelectColumn($alias . '.adtown_adm_weight');
            $criteria->addSelectColumn($alias . '.adtown_importance');
            $criteria->addSelectColumn($alias . '.adtown_created_at');
            $criteria->addSelectColumn($alias . '.adtown_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TownTableMap::DATABASE_NAME)->getTable(TownTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TownTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TownTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TownTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Town or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Town object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AddressBundle\Model\Town) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TownTableMap::DATABASE_NAME);
            $criteria->add(TownTableMap::COL_ADTOWN_ID, (array) $values, Criteria::IN);
        }

        $query = TownQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TownTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TownTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the address_town_adtown table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TownQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Town or Criteria object.
     *
     * @param mixed               $criteria Criteria or Town object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TownTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Town object
        }


        // Set the correct dbName
        $query = TownQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TownTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TownTableMap::buildTableMap();
