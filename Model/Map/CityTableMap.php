<?php

namespace IiMedias\AddressBundle\Model\Map;

use IiMedias\AddressBundle\Model\City;
use IiMedias\AddressBundle\Model\CityQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'address_city_adcity' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CityTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.AddressBundle.Model.Map.CityTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'address_city_adcity';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\AddressBundle\\Model\\City';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.AddressBundle.Model.City';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the adcity_id field
     */
    const COL_ADCITY_ID = 'address_city_adcity.adcity_id';

    /**
     * the column name for the adcity_adtype_id field
     */
    const COL_ADCITY_ADTYPE_ID = 'address_city_adcity.adcity_adtype_id';

    /**
     * the column name for the adcity_adcnty_id field
     */
    const COL_ADCITY_ADCNTY_ID = 'address_city_adcity.adcity_adcnty_id';

    /**
     * the column name for the adcity_name field
     */
    const COL_ADCITY_NAME = 'address_city_adcity.adcity_name';

    /**
     * the column name for the adcity_postcode field
     */
    const COL_ADCITY_POSTCODE = 'address_city_adcity.adcity_postcode';

    /**
     * the column name for the adcity_alternative_postcode_1 field
     */
    const COL_ADCITY_ALTERNATIVE_POSTCODE_1 = 'address_city_adcity.adcity_alternative_postcode_1';

    /**
     * the column name for the adcity_alternative_postcode_2 field
     */
    const COL_ADCITY_ALTERNATIVE_POSTCODE_2 = 'address_city_adcity.adcity_alternative_postcode_2';

    /**
     * the column name for the adcity_alternative_postcode_3 field
     */
    const COL_ADCITY_ALTERNATIVE_POSTCODE_3 = 'address_city_adcity.adcity_alternative_postcode_3';

    /**
     * the column name for the adcity_lat field
     */
    const COL_ADCITY_LAT = 'address_city_adcity.adcity_lat';

    /**
     * the column name for the adcity_lon field
     */
    const COL_ADCITY_LON = 'address_city_adcity.adcity_lon';

    /**
     * the column name for the adcity_pop field
     */
    const COL_ADCITY_POP = 'address_city_adcity.adcity_pop';

    /**
     * the column name for the adcity_adm_weight field
     */
    const COL_ADCITY_ADM_WEIGHT = 'address_city_adcity.adcity_adm_weight';

    /**
     * the column name for the adcity_importance field
     */
    const COL_ADCITY_IMPORTANCE = 'address_city_adcity.adcity_importance';

    /**
     * the column name for the adcity_housenumbers_json field
     */
    const COL_ADCITY_HOUSENUMBERS_JSON = 'address_city_adcity.adcity_housenumbers_json';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'TypeId', 'CountyId', 'Name', 'PostCode', 'AlternativePostCode1', 'AlternativePostCode2', 'AlternativePostCode3', 'Lat', 'Lon', 'Pop', 'AdmWeight', 'Importance', 'HouseNumbersJson', ),
        self::TYPE_CAMELNAME     => array('id', 'typeId', 'countyId', 'name', 'postCode', 'alternativePostCode1', 'alternativePostCode2', 'alternativePostCode3', 'lat', 'lon', 'pop', 'admWeight', 'importance', 'houseNumbersJson', ),
        self::TYPE_COLNAME       => array(CityTableMap::COL_ADCITY_ID, CityTableMap::COL_ADCITY_ADTYPE_ID, CityTableMap::COL_ADCITY_ADCNTY_ID, CityTableMap::COL_ADCITY_NAME, CityTableMap::COL_ADCITY_POSTCODE, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3, CityTableMap::COL_ADCITY_LAT, CityTableMap::COL_ADCITY_LON, CityTableMap::COL_ADCITY_POP, CityTableMap::COL_ADCITY_ADM_WEIGHT, CityTableMap::COL_ADCITY_IMPORTANCE, CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON, ),
        self::TYPE_FIELDNAME     => array('adcity_id', 'adcity_adtype_id', 'adcity_adcnty_id', 'adcity_name', 'adcity_postcode', 'adcity_alternative_postcode_1', 'adcity_alternative_postcode_2', 'adcity_alternative_postcode_3', 'adcity_lat', 'adcity_lon', 'adcity_pop', 'adcity_adm_weight', 'adcity_importance', 'adcity_housenumbers_json', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'TypeId' => 1, 'CountyId' => 2, 'Name' => 3, 'PostCode' => 4, 'AlternativePostCode1' => 5, 'AlternativePostCode2' => 6, 'AlternativePostCode3' => 7, 'Lat' => 8, 'Lon' => 9, 'Pop' => 10, 'AdmWeight' => 11, 'Importance' => 12, 'HouseNumbersJson' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'typeId' => 1, 'countyId' => 2, 'name' => 3, 'postCode' => 4, 'alternativePostCode1' => 5, 'alternativePostCode2' => 6, 'alternativePostCode3' => 7, 'lat' => 8, 'lon' => 9, 'pop' => 10, 'admWeight' => 11, 'importance' => 12, 'houseNumbersJson' => 13, ),
        self::TYPE_COLNAME       => array(CityTableMap::COL_ADCITY_ID => 0, CityTableMap::COL_ADCITY_ADTYPE_ID => 1, CityTableMap::COL_ADCITY_ADCNTY_ID => 2, CityTableMap::COL_ADCITY_NAME => 3, CityTableMap::COL_ADCITY_POSTCODE => 4, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1 => 5, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2 => 6, CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3 => 7, CityTableMap::COL_ADCITY_LAT => 8, CityTableMap::COL_ADCITY_LON => 9, CityTableMap::COL_ADCITY_POP => 10, CityTableMap::COL_ADCITY_ADM_WEIGHT => 11, CityTableMap::COL_ADCITY_IMPORTANCE => 12, CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON => 13, ),
        self::TYPE_FIELDNAME     => array('adcity_id' => 0, 'adcity_adtype_id' => 1, 'adcity_adcnty_id' => 2, 'adcity_name' => 3, 'adcity_postcode' => 4, 'adcity_alternative_postcode_1' => 5, 'adcity_alternative_postcode_2' => 6, 'adcity_alternative_postcode_3' => 7, 'adcity_lat' => 8, 'adcity_lon' => 9, 'adcity_pop' => 10, 'adcity_adm_weight' => 11, 'adcity_importance' => 12, 'adcity_housenumbers_json' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address_city_adcity');
        $this->setPhpName('City');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\AddressBundle\\Model\\City');
        $this->setPackage('src.IiMedias.AddressBundle.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('adcity_id', 'Id', 'VARCHAR', true, 255, null);
        $this->addForeignKey('adcity_adtype_id', 'TypeId', 'INTEGER', 'address_type_adtype', 'adtype_id', true, null, null);
        $this->addForeignKey('adcity_adcnty_id', 'CountyId', 'INTEGER', 'address_county_adcnty', 'adcnty_id', true, null, null);
        $this->addColumn('adcity_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('adcity_postcode', 'PostCode', 'VARCHAR', true, 255, null);
        $this->addColumn('adcity_alternative_postcode_1', 'AlternativePostCode1', 'VARCHAR', false, 255, null);
        $this->addColumn('adcity_alternative_postcode_2', 'AlternativePostCode2', 'VARCHAR', false, 255, null);
        $this->addColumn('adcity_alternative_postcode_3', 'AlternativePostCode3', 'VARCHAR', false, 255, null);
        $this->addColumn('adcity_lat', 'Lat', 'FLOAT', true, null, null);
        $this->addColumn('adcity_lon', 'Lon', 'FLOAT', true, null, null);
        $this->addColumn('adcity_pop', 'Pop', 'FLOAT', true, null, null);
        $this->addColumn('adcity_adm_weight', 'AdmWeight', 'FLOAT', true, null, null);
        $this->addColumn('adcity_importance', 'Importance', 'FLOAT', true, null, null);
        $this->addColumn('adcity_housenumbers_json', 'HouseNumbersJson', 'LONGVARCHAR', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Type', '\\IiMedias\\AddressBundle\\Model\\Type', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':adcity_adtype_id',
    1 => ':adtype_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('County', '\\IiMedias\\AddressBundle\\Model\\County', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':adcity_adcnty_id',
    1 => ':adcnty_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('Street', '\\IiMedias\\AddressBundle\\Model\\Street', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':adstrt_adcity_id',
    1 => ':adcity_id',
  ),
), 'CASCADE', 'CASCADE', 'Streets', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to address_city_adcity     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        StreetTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CityTableMap::CLASS_DEFAULT : CityTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (City object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CityTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CityTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CityTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CityTableMap::OM_CLASS;
            /** @var City $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CityTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CityTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CityTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var City $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CityTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ID);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ADTYPE_ID);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ADCNTY_ID);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_NAME);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_POSTCODE);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_1);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_2);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ALTERNATIVE_POSTCODE_3);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_LAT);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_LON);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_POP);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_ADM_WEIGHT);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_IMPORTANCE);
            $criteria->addSelectColumn(CityTableMap::COL_ADCITY_HOUSENUMBERS_JSON);
        } else {
            $criteria->addSelectColumn($alias . '.adcity_id');
            $criteria->addSelectColumn($alias . '.adcity_adtype_id');
            $criteria->addSelectColumn($alias . '.adcity_adcnty_id');
            $criteria->addSelectColumn($alias . '.adcity_name');
            $criteria->addSelectColumn($alias . '.adcity_postcode');
            $criteria->addSelectColumn($alias . '.adcity_alternative_postcode_1');
            $criteria->addSelectColumn($alias . '.adcity_alternative_postcode_2');
            $criteria->addSelectColumn($alias . '.adcity_alternative_postcode_3');
            $criteria->addSelectColumn($alias . '.adcity_lat');
            $criteria->addSelectColumn($alias . '.adcity_lon');
            $criteria->addSelectColumn($alias . '.adcity_pop');
            $criteria->addSelectColumn($alias . '.adcity_adm_weight');
            $criteria->addSelectColumn($alias . '.adcity_importance');
            $criteria->addSelectColumn($alias . '.adcity_housenumbers_json');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CityTableMap::DATABASE_NAME)->getTable(CityTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CityTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CityTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CityTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a City or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or City object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\AddressBundle\Model\City) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CityTableMap::DATABASE_NAME);
            $criteria->add(CityTableMap::COL_ADCITY_ID, (array) $values, Criteria::IN);
        }

        $query = CityQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CityTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CityTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the address_city_adcity table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CityQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a City or Criteria object.
     *
     * @param mixed               $criteria Criteria or City object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CityTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from City object
        }


        // Set the correct dbName
        $query = CityQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CityTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CityTableMap::buildTableMap();
