<?php

namespace IiMedias\AddressBundle\Command;

use IiMedias\AddressBundle\Model\BanoContent;
use IiMedias\AddressBundle\Model\HouseNumber;
use IiMedias\AddressBundle\Model\HouseNumberQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use IiMedias\AddressBundle\Model\Town;
use IiMedias\AddressBundle\Model\TownQuery;
use IiMedias\AddressBundle\Model\Street;
use IiMedias\AddressBundle\Model\StreetQuery;
use IiMedias\AddressBundle\Model\BanoQuery;
use IiMedias\AddressBundle\Model\Error;

class GetFrenchAddressesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('address:get:french')
            ->setDescription('Récupère toutes les adresses françaises et leurs localisations')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inUse = BanoQuery::create()
            ->filterByInUse(true)
            ->find()
            ->count();

        if ($inUse == 0) {
            $dept = BanoQuery::create()
                ->filterByScan(true)
                ->filterByInUse(false)
                ->findOne()
            ;



            if (!is_null($dept)) {
                $dept
                    ->setInUse(true)
                    ->save();

                $addrDlPath = realpath(
                    $this->getContainer()->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' .
                    DIRECTORY_SEPARATOR . 'ii' . DIRECTORY_SEPARATOR . 'addresses' . DIRECTORY_SEPARATOR . 'french'
                );


                //$output->writeln('Téléchargement du fichier pour le département ' . $dept);
//                $fileUrl = 'http://bano.openstreetmap.fr/data/bano-' . $dept->getDepartementCode() . '.json.gz';
//                $fileContent = file_get_contents($fileUrl);
//                file_put_contents($addrDlPath . DIRECTORY_SEPARATOR . 'bano-' . $dept->getDepartementCode() . '.json.gz', $fileContent);

                //$output->writeln('Décompression du fichier pour le département ' . $dept);
                $sh = 'gzip -d ' . $addrDlPath . DIRECTORY_SEPARATOR . 'bano-' . $dept->getDepartementCode() . '.json.gz';
                $process = new Process($sh);
                $process->run();

                //$output->writeln('Analyse pour le département ' . $dept);
                $fileContent = file_get_contents($addrDlPath . DIRECTORY_SEPARATOR . 'bano-' . $dept->getDepartementCode() . '.json');
                $fileContent = explode("\n", $fileContent);
                if ($fileContent[count($fileContent) - 1] == '') {
                    unset($fileContent[count($fileContent) - 1]);
                }
                $fileContent = implode(',', $fileContent);
                $fileContent = '[' . $fileContent . ']';
                $jsonContent = json_decode($fileContent, true);

                dump(count($jsonContent));

                foreach ($jsonContent as $jsonAddress) {
                    $banoContent = new BanoContent();
                    $banoContent
                        ->setType($jsonAddress['type'])
                        ->setContent(json_encode($jsonAddress))
                        ->setProcessed(false)
                        ->setError(false)
                        ->save()
                    ;
                }

                $dept
                    ->setInUse(false)
                    ->setScan(false)
                    ->save();
            }
        }
    }

}
