<?php

namespace IiMedias\AddressBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NodeJsGetConfigCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nodejs:address:getconfig')
            ->setDescription('Récupère la configuration du AddressBundle pour le serveur nodejs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = array(
            'repeat' => array(),
        );

        /* Gestion des commandes récurentes */
//        $config['repeat'][] = array(
//            'action'  => 'command',
//            'repeat'  => 60000,
//            'command' => 'address:get:french',
//            'args'    => array()
//        );
//        $config['repeat'][] = array(
//            'action'  => 'command',
//            'repeat'  => 60000,
//            'command' => 'address:work:french',
//            'args'    => array()
//        );
//        $config['repeat'][] = array(
//            'action'  => 'command',
//            'repeat'  => 60000,
//            'command' => 'address:numbers:french',
//            'args'    => array()
//        );

        $output->write(json_encode($config));
    }
}
