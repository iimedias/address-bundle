<?php

namespace IiMedias\AddressBundle\Command;

use IiMedias\AddressBundle\Model\BanoContent;
use IiMedias\AddressBundle\Model\BanoContentQuery;
use IiMedias\AddressBundle\Model\County;
use IiMedias\AddressBundle\Model\CountyQuery;
use IiMedias\AddressBundle\Model\Region;
use IiMedias\AddressBundle\Model\RegionQuery;
use IiMedias\AddressBundle\Model\Street;
use IiMedias\AddressBundle\Model\StreetQuery;
use IiMedias\AddressBundle\Model\Type;
use IiMedias\AddressBundle\Model\TypeQuery;
use IiMedias\AddressBundle\Model\City;
use IiMedias\AddressBundle\Model\CityQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class WorkFrenchAddressesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('address:work:french')
            ->setDescription('Travaille les adresses françaises et leurs localisations')
        ;
    }

    protected function verbose($verboseText) {
        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $this->output->writeln($verboseText);
        }
    }

    protected function veryVerbose($verboseText) {
        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
            $this->output->writeln($verboseText);
        }
    }

    protected function debugVerbose($verboseVar) {
        if ($this->output->getVerbosity() >= OutputInterface::VERBOSITY_DEBUG) {
            dump($verboseVar);
        }
    }

    protected function getBanoTypeId($typeName)
    {
        if (!isset($this->types[$typeName])) {
            $type = TypeQuery::create()
                ->filterByName($typeName)
                ->findOne()
            ;
            if (is_null($type)) {
                $type = new Type();
                $type
                    ->setName($typeName)
                    ->save()
                ;
            }
            $typeId                 = $type->getId();
            $this->types[$typeName] = $typeId;
        }
        else {
            $typeId = $this->types[$typeName];
        }
        return $typeId;
    }

    protected function getBanoRegionId($regionName) {
        if (!isset($this->regions[$regionName])) {
            $region = RegionQuery::create()
                ->filterByName($regionName)
                ->findOne();
            if (is_null($region)) {
                $region = new Region();
                $region
                    ->setName($regionName)
                    ->save()
                ;
            }
            $regionId                   = $region->getId();
            $this->regions[$regionName] = $regionId;
        }
        else {
            $regionId = $this->regions[$regionName];
        }
        return $regionId;
    }

    protected function getBanoCountyId($countyName, $regionId, $code)
    {
        if (!isset($this->counties[$countyName])) {
            $county = CountyQuery::create()
                ->filterByName($countyName)
                ->findOne();
            if (is_null($county)) {
                $county = new County();
                $county
                    ->setRegionId($regionId)
                    ->setName($countyName)
                    ->setCode($code)
                    ->save()
                ;
            }
            $countyId                    = $county->getId();
            $this->counties[$countyName] = $countyId;
        }
        else {
            $countyId = $this->counties[$countyName];
        }
        
        return $countyId;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output  = $output;
        $this->types   = array();
        $this->regions = array();
        $this->conties = array();
        $cities        = array();

        /* ****** *
         * VILLES *
         * ****** */

        $contents = BanoContentQuery::create()
            ->filterByType(array('town', 'city', 'village'), Criteria::IN)
            ->filterByProcessed(false)
            ->filterByError(false)
            ->limit(50)
            ->find();

        foreach ($contents as $content) {
            try {
                $jsonContent = json_decode($content->getContent(), true);

                /* Type */
                $typeName = $jsonContent['type'];
                $typeId = $this->getBanoTypeId($typeName);

                /* Région */
                $regionName = $jsonContent['region'];
                $regionId = $this->getBanoRegionId($regionName);

                /* Département */
                $countyName = $jsonContent['departement'];
                $countyId = $this->getBanoCountyId($countyName, $regionId, substr($jsonContent['postcode'], 0, 2));

                /* Ville */
                $cityName = $jsonContent['name'];
                $cityKey = $countyId . ' - ' . $cityName;
                if (!isset($cities[$cityKey])) {
                    $city = CityQuery::create()
                        ->filterByName($cityName)
                        ->filterByCountyId($countyId)
                        ->findOne();
                    if (is_null($city)) {
                        $city = new City();
                        $city
                            ->setId($jsonContent['id'])
                            ->setName($cityName)
                            ->setTypeId($typeId)
                            ->setCountyId($countyId)
                            ->setPostCode($jsonContent['postcode'])
                            ->setLat($jsonContent['lat'])
                            ->setLon($jsonContent['lon'])
                            ->setPop($jsonContent['population'])
                            ->setAdmWeight($jsonContent['adm_weight'])
                            ->setImportance($jsonContent['importance'])
                            ->save();
                    }
                    $cityId = $city->getId();
                    $cities[$cityKey] = $cityId;
                } else {
                    $cityId = $cities[$cityKey];
                }

                $content
                    ->setProcessed(true)
                    ->save();
            }
            catch (\Exception $e) {
                $content
                    ->setError(true)
                    ->save();
            }

//            dump($jsonContent, $typeId, $regionId, $countyId, $cityId, $cityKey);
        }

        /* **** *
         * RUES *
         * **** */

        $contents = BanoContentQuery::create()
            ->filterByType(array('street', 'place'), Criteria::IN)
            ->filterByProcessed(false)
            ->filterByError(false)
            ->limit(300)
            ->find();

        foreach ($contents as $content) {
            $jsonContent = json_decode($content->getContent(), true);

            /* Type */
            $typeName = $jsonContent['type'];
            $typeId   = $this->getBanoTypeId($typeName);

            /* Région */
            $regionName = $jsonContent['region'];
            $regionId   = $this->getBanoRegionId($regionName);

            /* Département */
            $countyName = $jsonContent['departement'];
            $countyId   = $this->getBanoCountyId($countyName, $regionId, null);

            /* Ville */
            $cityName = $jsonContent['city'];
            $cityKey  = $countyId . ' - ' . $cityName;
            if (!isset($cities[$cityKey])) {
                $city = CityQuery::create()
                    ->filterByName($cityName)
                    ->filterByCountyId($countyId)
                    ->findOne()
                ;

                if ($city->getPostCode() != $jsonContent['postcode']
                    && $city->getAlternativePostCode1() != $jsonContent['postcode']
                    && $city->getAlternativePostCode2() != $jsonContent['postcode']
                    && $city->getAlternativePostCode3() != $jsonContent['postcode']
                ) {
                    if ($city->getAlternativePostCode1() != $jsonContent['postcode'] && is_null($city->getAlternativePostCode1())) {
                        $city
                            ->setAlternativePostCode1($jsonContent['postcode'])
                            ->save();
                    }
                    elseif ($city->getAlternativePostCode2() != $jsonContent['postcode'] && is_null($city->getAlternativePostCode2())) {
                        $city
                            ->setAlternativePostCode2($jsonContent['postcode'])
                            ->save();
                    }
                    elseif ($city->getAlternativePostCode3() != $jsonContent['postcode'] && is_null($city->getAlternativePostCode3())) {
                        $city
                            ->setAlternativePostCode3($jsonContent['postcode'])
                            ->save();
                    }
                    else {
                        throw new \Exception('pas assez de postcode alternatif');
                    }
                }

                $cityId           = $city->getId();
                $cities[$cityKey] = $cityId;
            }
            else {
                $cityId = $cities[$cityKey];
            }

            /* Rue */
            $streetId   = $jsonContent['id'];
            $streetName = $jsonContent['name'];
            $street     = StreetQuery::create()
                ->filterById($streetId)
                ->findOne();
            if (is_null($street)) {
                $street = new Street();
                $street
                    ->setId($streetId)
                    ->setTypeId($typeId)
                    ->setCityId($cityId)
                    ->setName($streetName)
                    ->setPostCode($jsonContent['postcode'])
                    ->setLat($jsonContent['lat'])
                    ->setLon($jsonContent['lon'])
                    ->setImportance($jsonContent['importance'])
                    ->setHouseNumbersArray(isset($jsonContent['housenumbers']) ? json_encode($jsonContent['housenumbers']) : json_encode(array()))
                    ->save()
                ;
            }

            $content
                ->setProcessed(true)
                ->save()
            ;

//            dump($jsonContent, $typeId, $regionId, $countyId, $cityId, $street);
        }
    }

}
