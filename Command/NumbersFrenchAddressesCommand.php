<?php

namespace IiMedias\AddressBundle\Command;

use IiMedias\AddressBundle\Model\BanoContent;
use IiMedias\AddressBundle\Model\BanoContentQuery;
use IiMedias\AddressBundle\Model\County;
use IiMedias\AddressBundle\Model\CountyQuery;
use IiMedias\AddressBundle\Model\HouseNumber;
use IiMedias\AddressBundle\Model\HouseNumberQuery;
use IiMedias\AddressBundle\Model\Region;
use IiMedias\AddressBundle\Model\RegionQuery;
use IiMedias\AddressBundle\Model\Street;
use IiMedias\AddressBundle\Model\StreetQuery;
use IiMedias\AddressBundle\Model\Type;
use IiMedias\AddressBundle\Model\TypeQuery;
use IiMedias\AddressBundle\Model\City;
use IiMedias\AddressBundle\Model\CityQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class NumbersFrenchAddressesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('address:numbers:french')
            ->setDescription('Travaille les numéros de rues françaises et leurs localisations')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        StreetQuery::create()
            ->filterByHouseNumbersArray(json_encode(array()))
            ->update(array('HouseNumbersArray' => null));

        $streets = StreetQuery::create()
            ->filterByHouseNumbersArray(null, Criteria::NOT_EQUAL)
            ->limit(50)
            ->find()
        ;

        foreach ($streets as $street) {
            $houseNumbersArray = json_decode($street->getHouseNumbersArray(), true);

            foreach ($houseNumbersArray as $houseNumberKey => $houseNumberData) {
                $houseNumber = HouseNumberQuery::create()
                    ->filterByHouseNumber($houseNumberKey)
                    ->filterByStreet($street)
                    ->findOne()
                ;
                if (is_null($houseNumber)) {
                    $houseNumber = new HouseNumber();
                    $houseNumber
                        ->setHouseNumber($houseNumberKey)
                        ->setStreet($street)
                        ->setLat($houseNumberData['lat'])
                        ->setLon($houseNumberData['lon'])
                        ->save()
                    ;
                }
            }
            $street
                ->setHouseNumbersArray(null)
                ->save();
        }
    }
}
