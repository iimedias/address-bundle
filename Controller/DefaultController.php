<?php

namespace IiMedias\AddressBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IiMediasAddressBundle:Default:index.html.twig');
    }
}
